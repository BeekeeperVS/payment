<?php

use yii\db\Migration;

/**
 * Class m191016_110821_alert_accrual_table_delete_column_sum
 */
class m191016_110821_alert_accrual_table_delete_column_sum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%accrual}}', 'sum');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%accrual}}', 'sum', $this->integer()->notNull());
    }


}
