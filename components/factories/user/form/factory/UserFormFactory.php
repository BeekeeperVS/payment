<?php


namespace app\components\factories\user\form\factory;


use app\components\factories\user\form\UserForm;
use app\models\db\User;

class UserFormFactory implements UserFormFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function make(int $id = null): UserForm
    {
        return $id ? $this->findUser($id) : new UserForm();
    }

    private function findUser(int $id): UserForm
    {
        $model = User::findOne($id);

        $modelForm = new UserForm();
        $modelForm->id = $model->id;
        $modelForm->workId = $model->work_id;
        $modelForm->name = $model->name;
        $modelForm->surName = $model->surname;
        $modelForm->patronymic = $model->patronymic;
        $modelForm->workPosition = $model->work_position;
        $modelForm->subdivision = $model->subdivision;
        $modelForm->email = $model->email;
        $modelForm->login = $model->login;
//        $modelForm->password = $model->password;
        $modelForm->role = $model->role;
        $modelForm->isActive = $model->is_active;
        $modelForm->isDeleted = $model->is_deleted;


        return $modelForm;
    }
}