<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\form\DataUploadForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = translate('Upload 1С file');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accrual-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'period')->widget(DatePicker::class,
                [
                    'name' => 'dp_1',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'value' => date('m-Y'),
                    'pluginOptions' => [
                        'buttonText' => '',
                        'autoclose' => true,
                        'format' => 'mm-yyyy',
                        'startView' => 'year',
                        'minViewMode' => 'months',
                        'endDate' => date('m-Y')
                    ],
                    'options' => [
                        'class' => 'accrual-period',
                    ],
                ]
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'financeFile')->fileInput([
                'data' => [
                    'url' => Url::to(['/backend/upload-accrual/validate-file'])
                ],
                'class' => ['xls-file'],
            ]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton(
            translate('Save'), [
            'class' => 'btn btn-primary btn-lg btn-accrual-upload',
            'data' => [
                'url' => Url::to(['/backend/upload-accrual/validate-date']),
                'confirm' => ""
            ],
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
