<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\AccrualView */

$this->title = 'Create Accrual View';
$this->params['breadcrumbs'][] = ['label' => 'Accrual Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accrual-view-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
