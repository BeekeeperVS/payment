<?php

use app\components\factories\user\form\UserForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model UserForm */

$this->title = translate( 'Personal information').": ".$model->getFullName();
$this->params['breadcrumbs'][] = ['label' => translate('Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(translate('Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

//            'work_id',
            [
                'label' => 'ФИО',
                'value' => function (UserForm $model) {
                    return $model->getFullName();
                }
            ],

            'workPosition',
            'email:email',
            'login',
//            'password',

        ],
    ]) ?>

</div>
