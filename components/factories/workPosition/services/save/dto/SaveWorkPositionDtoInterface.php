<?php


namespace app\components\factories\workPosition\services\save\dto;


interface SaveWorkPositionDtoInterface
{

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string
     */
    public function getName(): string;

}