<?php


use app\assets\AccountantPageCryptoAsset;
use app\components\AccrualTypeEnum;
use app\components\factories\user\view\UserView;
use app\components\widgets\settingPagePagination\SettingPagePagination;
use app\models\form\AccrualFilterForm;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\LinkPager;

/* @var $this View */
/* @var $viewsModels UserView[] */
/* @var $filterForm AccrualFilterForm */
/* @var $pagination Pagination */

AccountantPageCryptoAsset::register($this);

$this->title = translate('Accruals');
$this->params['breadcrumbs'][] = $this->title;

$period = $filterForm->period;
$showTableAccruals = false;
$page = $_GET['page'] ?? 1;
?>
<div class="accrual-user-page">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <!--    <div class="container">-->

    <?= $this->render('_filter-accountant', ['filterForm' => $filterForm]) ?>

    <hr>
    <?= SettingPagePagination::widget(['pagination' => $pagination]) ?>
    <br>
    <?php foreach ($viewsModels as $model) {
        if (isset($model->accruals[$period])) {
            $showTableAccruals = true;
            break;
        }
    } ?>

    <?php if ($showTableAccruals): ?>
        <div class="table-responsive">
            <p class="table-accrual-name">
                Начисления за <?= $period ?>
            </p>
            <table class="table table-accrual table-accountant-crypto hide">

                <?php $index = 1 * $page; ?>
                <tr>
                    <th>#</th>
                    <th>Табельный номер</th>
                    <th>ФИО</th>
                    <?php foreach (UserView::getAccrualViewList() as $title) : ?>
                        <th> <?= $title ?></th>
                    <?php endforeach; ?>
                    <th>Удержано</th>
                    <th>Итого</th>
                </tr>

                <?php foreach ($viewsModels as $model) : ?>
                    <?php if (!isset($model->accruals[$period])) continue; ?>
                    <!--                --><?php //print_r($model); die;?>
                    <tr>
                        <td><?= $index++ ?></td>
                        <td><?= $model->getWorkId() ?></td>
                        <td><?= $model->getFullName() ?></td>
                        <?php foreach (UserView::getAccrualViewList() as $title) : ?>
                            <?php $value = $model->accruals[$period][$title][AccrualTypeEnum::ACCRUAL] ?? '0'; ?>
                            <td <?php if ($value): ?>
                                class="accrual-decrypt" data-crypto-key="<?= $value ?>"
                            <?php endif; ?> >
                                0
                                <!--                                    /-->
                                <!--                                    --><? //= $model->accruals[$period][$title][AccrualTypeEnum::DEDUCTION] ?? '0'; ?>
                            </td>
                        <?php endforeach; ?>
                        <td><?= $model->sum[$period][AccrualTypeEnum::DEDUCTION] ?></td>
                        <td class="accrual-decrypt" data-crypto-key="<?= $model->workId . '_' . $period ?>_sum">
                            <!--                            --><? //= $model->sum[$period][AccrualTypeEnum::ACCRUAL] ?>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-6 not-find">
                <p>По вашему запросу ничего не найдено</p>
            </div>
        </div>
    <?php endif; ?>
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>

    <!--    </div>-->
</div>
