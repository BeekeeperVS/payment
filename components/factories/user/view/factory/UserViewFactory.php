<?php


namespace app\components\factories\user\view\factory;


use app\components\factories\user\view\UserView;
use app\models\db\User;
use yii\data\Pagination;
use yii\data\Sort;


class UserViewFactory implements UserViewFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function make(int $id, bool $cipher = false, array $filterAccrualsParams = []): UserView
    {
        $user = User::findOne($id);

        return new UserView(
            $user->id,
            $user->work_id,
            $user->name,
            $user->surname,
            $user->patronymic ?? '',
            $user->workPosition->name ?? '',
            $cipher,
            $filterAccrualsParams
        );
    }

    /**
     * @inheritDoc
     */
    public function makeByUsersId(array $usersId): array
    {
        /* @var $usersId User[] */

        $usersId = User::find()
            ->where(['in', 'work_id', $usersId])
            ->orderBy('surname ASC, name ASC, patronymic ASC')
            ->all();
        $usersViewsList = [];
        foreach ($usersId as $user) {
            $usersViewsList[] = new UserView(
                $user->id,
                $user->work_id,
                $user->name,
                $user->surname,
                $user->patronymic ?? '',
                $user->workPosition->name ?? ''
            );
        }

        return $usersViewsList;

    }

    /**
     * @inheritDoc
     */
    public function makeAll(string $name = null, int $user_work_id = null, $period = null): array
    {
        /* @var $users User[] */
        if (!$user_work_id) {
            $users = User::find()->
            joinWith('accruals')
                ->andWhere(['accrual.period' => $period])
                ->andFilterWhere([
                    'or',
                    ['like', 'user.name', $name],
                    ['like', 'user.surname', $name],
                    ['like', 'user.patronymic', $name],
                ]);

            /** Pagination block */
            $userCount = $users->groupBy('user.id')->count();
            $pagination = new Pagination(['totalCount' => $userCount]);
            $pagination->defaultPageSize = 10;

            $users = $users
                ->orderBy('surname ASC, name ASC, patronymic ASC')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            $usersViewsList = [];

            foreach ($users as $user) {
                $usersViewsList[] = new UserView(
                    $user->id,
                    $user->work_id,
                    $user->name,
                    $user->surname,
                    $user->patronymic ?? '',
                    $user->workPosition->name ?? ''
                );
            }

            return ['models' => $usersViewsList, 'pagination' => $pagination];
        } else {
            /**@var User $user */
            if ($user = User::find()->where(['work_id' => $user_work_id])->one()) {
                $usersViewsList = [
                    new UserView(
                        $user->id,
                        $user->work_id,
                        $user->name,
                        $user->surname,
                        $user->patronymic ?? '',
                        $user->workPosition->name ?? ''
                    )
                ];
                return ['models' => $usersViewsList, 'pagination' => new Pagination()];
            }
        }

    }

}