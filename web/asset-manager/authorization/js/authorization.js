var Authorization = function () {
    return {
        init: init
    };

    function init() {

        if ($('#popup .modal-body .alert').text()) {
            $('#popup').modal('show');
        }

        $(document).mousedown(function () {
            $('#restorepasswordform-email').trigger("change");
        });
        $(document).keyup(function () {
            $('#restorepasswordform-email').trigger("change");
        });

        $('.btn-restore-password').on('click', function () {
            if(!$('.field-restorepasswordform-email .help-block-error').text()){
                var $this = $(this);
                $this.attr('disabled', true);
                var url = $this.data('url');
                var urlHome = $this.data('url-home');
                var form = $('#restore-form');
                formData = new FormData(form.get(0));
                $.ajax({
                    contentType: false,
                    processData: false,
                    data: formData,
                    url: url,
                    cache: false,
                    type: 'POST',
                    success: function (data) {
                        window.location = urlHome;
                    }
                })
            }
        })
    }

}();

Authorization.init();