<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\AccrualView */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accrual-view-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-7">
            <?= $form->field($model, 'cipher')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(translate('Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
