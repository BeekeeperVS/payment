<?php


namespace app\components\factories\workPosition\form\factory;


use app\components\workPosition\exception\WorkPositionException;
use app\components\factories\workPosition\form\WorkPositionForm;

interface WorkPositionFormFactoryInterface
{
    /**
     * @param int|null $id
     * @return WorkPositionForm
     * @throws WorkPositionException
     */
    public function make(int $id = null): WorkPositionForm;
}