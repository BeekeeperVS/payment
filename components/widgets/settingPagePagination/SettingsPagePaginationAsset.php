<?php


namespace app\components\widgets\settingPagePagination;


use yii\web\AssetBundle;

class SettingsPagePaginationAsset extends AssetBundle
{
    public $basePath = '@webroot/';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $css = [

    ];
    public $js = [
        'asset-manager/admin/js/pagination.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}