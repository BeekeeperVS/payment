<?php


namespace app\components\factories\workPosition\form;


use app\components\factories\workPosition\services\save\dto\SaveWorkPositionDto;
use app\components\factories\workPosition\services\save\dto\SaveWorkPositionDtoInterface;
use app\models\db\WorkPosition;
use yii\base\Model;

class WorkPositionForm extends Model implements WorkPositionFormInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'in', 'not' => true,
                'range' => $this->getListValidateUnique(),
                'message' => translate('Такая должность уже существует')
            ],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'id' => 'ID',
            'name' => 'Work Position',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return SaveWorkPositionDtoInterface
     */
    public function makeDto(): SaveWorkPositionDtoInterface
    {
        return new SaveWorkPositionDto(
            $this->id,
            $this->name
        );
    }

    /**
     * @return string[]
     */
    private function getListValidateUnique(): array
    {
        $list = WorkPosition::find()->select('name')->column();

        if ($this->getId()) {
            $nameCurrent = (WorkPosition::find()->select('name')->where(['id' => $this->getId()])->column())[0];
            if (($key = array_search($nameCurrent, $list)) !== false) {
                unset($list[$key]);
            }
        }

        return $list;
    }
}