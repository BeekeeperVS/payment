<?php
/**
 * Created by PhpStorm.
 * User: beeke
 * Date: 26.06.2019
 * Time: 15:40
 */

namespace app\components\gridView;

use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;


class ActionFilterColumn extends ActionColumn
{

    public $filterButtons = true;
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->header = translate('Actions');
    }

    protected function renderFilterCellContent()
    {
        $buttons = [
            'submit' => Html::submitButton(translate('Find'), ['class'=>"btn btn-primary btn-lg submit"]),
            'reset' =>  Html::a( translate('Reset'), ['index'], ['class' => 'btn btn-default btn-lg reset']),//Html::resetButton(Yii::t('backend', 'Reset'), ['class'=>"btn btn-default reset"]),
        ];
        if ($this->filterButtons) {
            return $buttons['submit'].' '.$buttons['reset'];
        }

    }

}