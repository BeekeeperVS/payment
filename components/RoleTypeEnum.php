<?php


namespace app\components;


abstract class RoleTypeEnum
{
    const ADMIN = 1;
    const FINANCE_MANAGER = 2;
    const HR_MANAGER = 3; //создавать и удалять пользователей
    const USER = 4;

    /**
     * @return array
     */
    public static function getRoleList(): array
    {
        return [
            self::USER => translate('User'),
            self::ADMIN => translate('Admin'),
            self::FINANCE_MANAGER => translate('Finance Manager'),
            self::HR_MANAGER => translate('HR Manager'),
        ];
    }

    /**
     * @param int $roleId
     * @return string
     */
    public static function getRoleTitle(int $roleId): string
    {
        $rolesList = self::getRoleList();

        return (string)$rolesList[$roleId];
    }
}