<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;


use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot/';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $css = [
        'asset-manager/admin/css/admin.css',
        'asset-manager/admin/css/admin_new.css',
    ];
    public $js = [
        'asset-manager/admin/js/admin.js',
        'asset-manager/admin/js/admin_new.js',
        'asset-manager/admin/js/selectsize.js',
        'asset-manager/admin/js/setPrivet.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
