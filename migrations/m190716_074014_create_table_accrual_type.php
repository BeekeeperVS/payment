<?php

use yii\db\Migration;

/**
 * Class m190716_074014_create_table_accrual_type
 */
class m190716_074014_create_table_accrual_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%accrual_type}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'alias' => $this->string(100)->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
            'updated_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
        ], $tableOptions);

        $this->createIndex('idx-accrual_type-title', '{{%accrual_type}}', 'title', true);
        $this->createIndex('idx-accrual_type-alias', '{{%accrual_type}}', 'alias',true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropTable('{{%accrual_type}}');

    }

}
