<?php


namespace app\components\factories\accrual\services\save\dto;


class SaveAccrualDto implements SaveAccrualDtoInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $userWorkId;
    /**
     * @var string
     */
    private $accrualType;
    /**
     * @var int
     */
    private $accrualViewId;
    /**
     * @var string
     */
    private $comment;
    /**
     * @var string
     */
    private $period;


    /**
     * SaveAccrualDto constructor.
     * @param int|null $id
     * @param int $userWorkId
     * @param string $accrualType
     * @param int $accrualViewId
     * @param string $comment
     * @param string $period
     */
    public function __construct(
        ?int $id,
        int $userWorkId,
        string $accrualType,
        int $accrualViewId,
        string $comment,
        string $period
    )
    {
        $this->id = $id;
        $this->userWorkId = $userWorkId;
        $this->accrualType = $accrualType;
        $this->accrualViewId = $accrualViewId;
        $this->comment = $comment;
        $this->period = $period;
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getUserWorkId(): int
    {
        return $this->userWorkId;
    }

    /**
     * @inheritDoc
     */
    public function getAccrualType(): string
    {
        return $this->accrualType;
    }

    /**
     * @inheritDoc
     */
    public function getAccrualViewId(): int
    {
        return $this->accrualViewId;
    }

    /**
     * @inheritDoc
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @inheritDoc
     */
    public function getPeriod(): string
    {
        return $this->period;
    }



}