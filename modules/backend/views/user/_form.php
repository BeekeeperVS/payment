<?php

use app\components\factories\user\form\UserForm;
use app\components\RoleTypeEnum;
use app\components\SubdivisionEnum;
use app\components\WorkPositionEnum;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model UserForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $showReplacePassword bool */
?>
<?php

$identity = Yii::$app->user->identity;
$disabledParameterFromUser = [];
if ($identity->whoIs([RoleTypeEnum::USER])) {
    $disabledParameterFromUser = true;
} else {
    $disabledParameterFromUser = false;
}
$classHide = '';
$model->password_repeat = $model->password;
if ($model->id && !$showReplacePassword) {
    $classHide = 'hide';
}

?>
<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'name')->textInput([
                'maxlength' => true,
                'disabled' => $disabledParameterFromUser,
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'surName')->textInput([
                'maxlength' => true,
                'disabled' => $disabledParameterFromUser
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'patronymic')->textInput([
                'maxlength' => true,
                'disabled' => $disabledParameterFromUser
            ]) ?>
        </div>
        <!--        <div class="col-md-6 col-lg-4">-->
        <!--            --><? //= $form->field($model, 'workId')->textInput([
        //                'maxlength' => true,
        //                'disabled' => $disabledParameterFromUser
        //            ]) ?>
        <!--        </div>-->
        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'workPosition')->dropDownList(WorkPositionEnum::getWorkPositionList(), [
                'class' => 'selectize-role',
                'disabled' => $disabledParameterFromUser
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'subdivision')->dropDownList(SubdivisionEnum::getSubdivisionList(), [
                'class' => 'selectize-role',
                'disabled' => $disabledParameterFromUser
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <?php if (Yii::$app->user->identity->whoIs([RoleTypeEnum::ADMIN])) : ?>
            <div class="col-md-6 col-lg-4">
                <?= $form->field($model, 'role')->dropDownList(RoleTypeEnum::getRoleList(), [
                    'class' => 'selectize-role'
                ]) ?>
            </div>
        <?php endif; ?>

        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'login')->textInput([
                'maxlength' => true,
                'autocomplete' => 'newLogin'
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-4 password <?= $classHide ?>">
            <?= $form->field($model, 'password')->passwordInput([
                'maxlength' => true,
                'class' => 'form-control',
                'autocomplete' => 'newPassword'
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-4 password <?= $classHide ?>">
            <?= $form->field($model, 'password_repeat')->passwordInput([
                'maxlength' => true,
                'autocomplete' => 'newPasswordRepeat'
            ]) ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton(translate('Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
