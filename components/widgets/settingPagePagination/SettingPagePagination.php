<?php


namespace app\components\widgets\settingPagePagination;


use Yii;
use yii\base\Widget;
use yii\data\Pagination;

class SettingPagePagination extends Widget
{
    /**
     * @var Pagination
     */
    public $pagination;

    public function run()
    {
        return $this->render('settings-page-pagination', [
            'pagination' => $this->pagination,
        ]);
    }
}