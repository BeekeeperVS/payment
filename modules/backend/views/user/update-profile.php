<?php

use app\components\factories\user\form\UserForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model UserForm */

$this->title = translate('Update Profile');
$this->params['breadcrumbs'][] = ['label' => translate('Profile'), 'url' => ['profile']];
$this->params['breadcrumbs'][] = translate('Update Profile');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-user', [
        'model' => $model,
        'showReplacePassword' => true
    ]) ?>

</div>
