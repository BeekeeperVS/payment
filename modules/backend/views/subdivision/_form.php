<?php

use app\components\factories\subdivision\form\factory\SubdivisionFormFactory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model SubdivisionFormFactory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subdivision-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(translate('Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
