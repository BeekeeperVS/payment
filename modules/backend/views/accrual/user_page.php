<?php


use app\assets\UserPageCryptoAsset;
use app\components\factories\user\view\UserView;
use app\components\widgets\settingPagePagination\SettingPagePagination;
use app\models\form\PeriodFilterForm;
use kartik\date\DatePicker;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

/* @var $this View */
/* @var $viewModel UserView */
/* @var $periodModel PeriodFilterForm */
/* @var $periods string[] */
/* @var $pagination Pagination */

$this->title = translate('User account');
$this->params['breadcrumbs'][] = $this->title;
UserPageCryptoAsset::register($this);

$layout = <<< HTML
                <label> c </label>
                {input1}
                <label style="margin: 0 10px"> по </label>
                {input2}
HTML;
?>

<div class="accrual-user-page">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <!--    <div class="container">-->
    <p>Период</p>
    <hr>
    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'action' => Url::to(['/backend/accrual/filter', 'per-page' => $_GET['per-page'] ?? null])
    ]); ?>

    <div class="period">
        <div class="period-item period">
            <?= $form->field($periodModel, 'period_from')->widget(DatePicker::class, [
                'type' => DatePicker::TYPE_RANGE,
                'attribute2' => 'period_to',
                'layout' => $layout,
                'options' => [
                    'style' => 'border-radius: 4px;'
                ],
                'options2' => [
                    'style' => 'border-radius: 4px;'
                ],
                'pluginOptions' => [
                    'buttonText' => '',
                    'autoclose' => true,
                    'format' => 'mm-yyyy',
                    'startView' => 'year',
                    'minViewMode' => 'months',
                    'endDate' => date('m-Y')
                ],
            ])->label(false);
            ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Показать', ['class' => 'btn btn-primary btn-lg']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <hr>
    <?= SettingPagePagination::widget(['pagination' => $pagination]) ?>
    <br>
    <div class="row">
        <?php if (count($viewModel->accruals)) : ?>
            <?php for ($i = $pagination->offset; $i < ($pagination->offset + $pagination->limit); $i++): ?>
                <?= $this->render('_payment_item', [
                    'viewModel' => $viewModel,
                    'period' => $viewModel->getPeriodByIndex($i),
                ]);
                ?>
            <?php endfor; ?>
        <?php else: ?>
            <div class="col-md-12 not-find">
                <p>По вашему запросу ничего не найдено</p>
            </div>
        <?php endif ?>
    </div>
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination
    ]);
    ?>
    <!--    </div>-->
</div>
