<?php

use app\components\factories\user\form\UserForm;
use app\components\gridView\DataColumnFilter;
use app\components\gridView\DataColumnSelect2Filter;
use app\components\RoleTypeEnum;
use app\components\widgets\settingPagePagination\SettingPagePagination;
use app\models\db\User;
use app\models\db\WorkPosition;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = translate('Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (!Yii::$app->user->identity->whoIs([
        RoleTypeEnum::FINANCE_MANAGER,
    ])) : ?>
        <p>
            <?= Html::a(translate('Create User'), ['create'], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>
    <?= SettingPagePagination::widget(['pagination' => $dataProvider->pagination]) ?>

    <?php if (Yii::$app->user->identity->whoIs([
        RoleTypeEnum::FINANCE_MANAGER,
    ])) : ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [
                'class' => 'table-responsive',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'format' => 'raw',
                    'attribute' => 'work_id',
                    'contentOptions' => [
                        'class' => 'cell-user-workId'
                    ],
                    'value' => function (User $model) {
                        $inputClassHide = $model->work_id ? 'hide' : '';
                        $spanClassHide = $model->work_id ? '' : 'hide';
                        return
                            Html::input('text', '', $model->work_id, [
                                'class' => [$inputClassHide, 'form-control', 'input-user-workId', 'workId-user-' . $model->id,],
                                'data' => [
                                    'user' => $model->id,
                                ],
                                'size' => 10
                            ]) .
                            Html::tag('span', $model->work_id, [
                                'class' => [$spanClassHide, 'span-user-workId', 'workId-user-' . $model->id],
                                'data' => [
                                    'user' => $model->id,
                                ],
                            ]);
                    },
                ],
                'name',
                'surname',
                'patronymic',
                [
                    'class' => DataColumnSelect2Filter::class,
                    'filter' => $searchModel->getWorkPositionsList(),
                    'attribute' => 'work_position',
                    'value' => function (User $model) {
                        return $model->workPosition->name ?? translate('Не задано');
                    }
                ],
                [
                    'class' => \app\components\gridView\ActionFilterColumn::class,
                    'template' => '{update} {save}',
                    'headerOptions' => [
                        'class' => 'users-action-th',
                    ],
                    'contentOptions' => [
                        'class' => 'text-center users-actions',
                    ],
                    'buttons' => [
                        'update' => function ($url, User $model, $key) {
                            $editClassHide = $model->work_id ? '' : 'hide';
                            return Html::button(
                                '<span class="glyphicon glyphicon-pencil update-workId"></span>',
                                [
                                    'class' => [$editClassHide, 'edit-workId-btn', 'edit-workId-user-' . $model->id, 'update-workId'],
                                    'title' => translate('Редактировать табельный номер'),
                                    'data' => [
                                        'user' => $model->id,
                                    ],

                                ]);
                        },
                        'save' => function ($url, User $model, $key) {
                            $saveClassHide = $model->work_id ? 'hide' : '';
                            return Html::button(
                                '<span class="glyphicon glyphicon-floppy-disk update-workId"></span>',
                                [
                                    'class' => [$saveClassHide, 'save-workId-btn', 'save-workId-user-' . $model->id, 'update-workId'],
                                    'title' => translate('Save'),
                                    'data' => [
                                        'user' => $model->id,
                                        'url' => Url::to(['/backend/user/edit-work-id']),
                                    ],

                                ]);
                        },
                    ],
                ],
            ]
        ]); ?>
    <?php elseif (Yii::$app->user->identity->whoIs([
        RoleTypeEnum::ADMIN,
    ])) : ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [
                'class' => 'table-responsive',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'work_id',
                'name',
                'surname',
                'patronymic',
                [
                    'class' => DataColumnSelect2Filter::class,
                    'filter' => $searchModel->getWorkPositionsList(),
                    'attribute' => 'work_position',
                    'value' => function (User $model) {
                        return $model->workPosition->name ?? translate('Не задано');
                    },
                    'headerOptions' => [
                        'style' => 'min-width: 200px',
                    ]
                ],
                [
                    'class' => DataColumnSelect2Filter::class,
                    'filter' => RoleTypeEnum::getRoleList(),
                    'attribute' => 'role',
                    'label' => translate('Role'),
                    'value' => function (User $model) {
                        return RoleTypeEnum::getRoleTitle($model->role);
                    },
                    'headerOptions' => [
                        'style' => 'min-width: 150px',
                    ]
                ],
                [
                    'format' => 'raw',
                    'label' => translate('Active'),
                    'value' => function (User $model) {
                        $active = Html::button(translate('Active'),
                            [
                                'class' => "btn btn-lg btn-primary btn-active",
                                'data' => [
                                    'url' => Url::to(['/backend/user/active', 'id' => $model->id])
                                ],

                            ]);
                        $block = Html::button(translate('Block'),
                            [
                                'class' => "btn btn-lg btn-default btn-active",
                                'data' => [
                                    'url' => Url::to(['/backend/user/active', 'id' => $model->id])
                                ],

                            ]);
                        return $model->is_active ? $block : $active;
                    },
                ],
                [
                    'class' => \app\components\gridView\ActionFilterColumn::class,
                    'template' => '{update} {view} {in_basket}',
                    'filterOptions' => [
                        'class' => 'd-flex filter-actions',
                    ],
                    'headerOptions' => [
                        'class' => 'users-action-th',
                    ],
                    'contentOptions' => [
                        'class' => 'text-center users-actions',
                    ],
                    'buttons' => [
                        'in_basket' => function ($url, User $model, $key) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-remove"></span>',
                                ['/backend/user/in-basket', 'id' => $model->id],
                                [
                                    'class' => ['action-button'],
                                    'title' => translate('In Basket'),

                                ]);
                        },
                    ],
                ],
            ]
        ]); ?>
    <?php else : ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [
                'class' => 'table-responsive',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'work_id',
                'name',
                'surname',
                'patronymic',
                [
                    'class' => DataColumnSelect2Filter::class,
                    'filter' => $searchModel->getWorkPositionsList(),
                    'attribute' => 'work_position',
                    'value' => function (User $model) {
                        return $model->workPosition->name ?? translate('Не задано');
                    },
                    'headerOptions' => [
                        'style' => 'min-width: 200px',
                    ]
                ],

                [
                    'format' => 'raw',
                    'label' => translate('Active'),
                    'value' => function (User $model) {
                        $active = Html::button(translate('Active'),
                            [
                                'class' => "btn btn-lg btn-primary btn-active",
                                'data' => [
                                    'url' => Url::to(['/backend/user/active', 'id' => $model->id])
                                ],

                            ]);
                        $block = Html::button(translate('Block'),
                            [
                                'class' => "btn btn-lg btn-default btn-active",
                                'data' => [
                                    'url' => Url::to(['/backend/user/active', 'id' => $model->id])
                                ],

                            ]);
                        return $model->is_active ? $block : $active;
                    },
                ],
                [
                    'class' => \app\components\gridView\ActionFilterColumn::class,
                    'template' => '{update} {view} {in_basket}',
                    'filterOptions' => [
                        'class' => 'd-flex filter-actions',
                    ],
                    'headerOptions' => [
                        'class' => 'users-action-th',
                    ],
                    'contentOptions' => [
                        'class' => 'text-center users-actions',
                    ],
                    'buttons' => [
                        'in_basket' => function ($url, User $model, $key) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-remove"></span>',
                                ['/backend/user/in-basket', 'id' => $model->id],
                                [
                                    'class' => ['action-button'],
                                    'title' => translate('In Basket'),

                                ]);
                        },
                    ],
                ],
            ]
        ]); ?>
    <?php endif; ?>
</div>
