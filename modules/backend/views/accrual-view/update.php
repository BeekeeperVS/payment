<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\AccrualView */

$this->title = 'Редактировать шифр: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => translate('Accrual Views'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать шифр: ' . $model->title;
?>
<div class="accrual-view-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
