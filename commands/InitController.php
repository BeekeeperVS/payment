<?php


namespace app\commands;

use app\models\db\AccrualType;
use app\models\db\AccrualView;
use app\models\db\Subdivision;
use app\models\db\User;
use app\models\db\WorkPosition;
use yii\console\Controller;

class InitController extends Controller
{

    public function actionIndex()
    {
        print_r("Start init...\n");
        print_r("------------------------\n");

        $this->initAccrualType();

        $this->initAccrualView();

        $this->initWorkPosition();

        $this->initSubdivision();

        $this->initUsers();

        print_r("Finish init!\n");
    }

    public function actionAccrualType()
    {
        $this->initAccrualType();
    }

    public function actionAccrualView()
    {
        $this->initAccrualView();
    }

    public function actionWorkPosition()
    {
        $this->initWorkPosition();
    }

    public function actionSubdivision()
    {
        $this->initSubdivision();
    }

    public function actionUsers()
    {
        $this->initUsers();
    }

    private function initAccrualType()
    {
        print_r("Start init accrual type...\n");
        $accrualTypesList = require __DIR__ . '/../components/init/accrual-type.php';
        foreach ($accrualTypesList as $accrualTypeList) {
            $model = new AccrualType();
            $model->title = $accrualTypeList['title'];
            $model->alias = $accrualTypeList['alias'];
            $model->save();
        }
        print_r("End init accrual type...\n");
        print_r("------------------------\n");
    }

    private function initAccrualView()
    {
        print_r("Start init accrual view...\n");
        $accrualViewsList = require __DIR__ . '/../components/init/accrual-view.php';
        foreach ($accrualViewsList as $accrualViewList) {
            $model = new AccrualView();
            $model->title = $accrualViewList['title'];
            $model->cipher = $accrualViewList['cipher'];
            $model->type_alias = $accrualViewList['type_alias'];
            $model->save();
        }
        print_r("End init accrual view...\n");
        print_r("------------------------\n");
    }

    private function initUsers()
    {
        print_r("Start init users...\n");
        $users = require __DIR__ . '/../components/init/user.php';
        foreach ($users as $user) {
            $model = new User();
            $model->name = $user['name'];
            $model->surname = $user['surname'];
            $model->patronymic = $user['patronymic'];
            $model->work_id = $user['work_id'];
            $model->role = $user['role'];
            $model->login = $user['login'];
            $model->email = $user['email'];
            $model->password = $user['password'];
            $model->first_password = $user['first_password'];
            $model->is_active = $user['is_active'];
            $model->save();
        }
        print_r("End init accrual users...\n");
        print_r("------------------------\n");
    }

    private function initWorkPosition()
    {
        print_r("Start init work positions...\n");
        $workPositions = require __DIR__ . '/../components/init/work-position.php';
        foreach ($workPositions as $workPosition) {
            $model = new WorkPosition();
            $model->name = $workPosition;
            $model->save();
        }
        print_r("End init accrual work position...\n");
        print_r("------------------------\n");
    }

    private function initSubdivision()
    {
        print_r("Start init work subdivisions...\n");
        $subdivisions = require __DIR__ . '/../components/init/subdivision.php';
        foreach ($subdivisions as $subdivision) {
            $model = new Subdivision();
            $model->name = $subdivision;
            $model->save();
        }
        print_r("End init accrual work position...\n");
        print_r("------------------------\n");
    }
}