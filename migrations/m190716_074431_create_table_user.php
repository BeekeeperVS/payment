<?php

use yii\db\Migration;

/**
 * Class m190716_074431_create_table_user
 */
class m190716_074431_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'work_id' => $this->integer()->null(),
            'name' => $this->string(100)->notNull(),
            'surname' => $this->string(100)->notNull(),
            'patronymic' => $this->string(100)->null(),
            'work_position' => $this->string(100)->null(),
            'email' => $this->string(100)->null(),
            'login' => $this->string(100)->null(),
            'password' => $this->string(100)->null(),
            'role' => $this->integer()->notNull(),
            'is_active' => $this->integer()->notNull()->defaultValue(0),
            'is_deleted' => $this->integer()->notNull()->defaultValue(0),
            'first_password' => $this->string()->null(),
            'created_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
            'updated_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
        ], $tableOptions);

        $this->createIndex('idx-user-work_id', '{{%user}}', 'work_id');
        $this->createIndex('idx-user-name', '{{%user}}', 'name');
        $this->createIndex('idx-user-surname', '{{%user}}', 'surname');
        $this->createIndex('idx-user-patronymic', '{{%user}}', 'patronymic');
        $this->createIndex('idx-user-work_position', '{{%user}}', 'work_position');
        $this->createIndex('idx-user-email', '{{%user}}', 'email', true);
        $this->createIndex('idx-user-login', '{{%user}}', 'login', true);
        $this->createIndex('idx-user-password', '{{%user}}', 'password');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
