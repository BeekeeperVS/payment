<?php


namespace app\components\factories\accrual\services\save;


use app\components\factories\accrual\exception\AccrualException;
use app\components\factories\accrual\services\save\dto\SaveAccrualDtoInterface;
use app\models\db\Accrual;

class SaveAccrualService implements SaveAccrualServiceInterface
{

    /**
     * @param SaveAccrualDtoInterface $dto
     * @return int
     * @throws AccrualException
     */
    public function save(SaveAccrualDtoInterface $dto): int
    {
        $model = $dto->getId() ? $this->findAccrual($dto) : new Accrual();

        $this->updateAccrual($dto, $model);

        return $model->id;
    }

    /**
     * @param SaveAccrualDtoInterface $dto
     * @return Accrual
     * @throws AccrualException
     */
    private function findAccrual(SaveAccrualDtoInterface $dto): Accrual
    {
        $model = Accrual::findOne($dto->getId());
        if ($model) {
            return $model;
        } else {
            throw new AccrualException(SaveAccrualServiceInterface::class . ' Failed to find Accrual by id: ' . $dto->getId());
        }
    }

    /**
     * @param SaveAccrualDtoInterface $dto
     * @param Accrual $model
     */
    private function updateAccrual(SaveAccrualDtoInterface $dto, Accrual $model)
    {

        $model->user_work_id = $dto->getUserWorkId();
        $model->accrual_type = $dto->getAccrualType();
        $model->accrual_view_id = $dto->getAccrualViewId();
        $model->comment = $dto->getComment();
        $model->period = $dto->getPeriod();
        if(!$model->id){
            $model->default_user_work_id = $dto->getUserWorkId();
        }
        $model->save();

    }
}