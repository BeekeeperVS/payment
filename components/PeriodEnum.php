<?php


namespace app\components;


abstract class PeriodEnum
{
    const JANUARY = '01';
    const FEBRUARY = '02';
    const MARCH = '03';
    const APRIL = '04';
    const MAY = '05';
    const JUNE = '06';
    const JULY = '07';
    const AUGUST = '08';
    const SEPTEMBER = '09';
    const OCTOBER = '10';
    const NOVEMBER = '11';
    const DECEMBER = '12';

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            self::JANUARY => 'январь',
            self::FEBRUARY => 'февраль',
            self::MARCH => 'март',
            self::APRIL => 'апрель',
            self::MAY => 'май',
            self::JUNE => 'июнь',
            self::JULY => 'июль',
            self::AUGUST => 'август',
            self::SEPTEMBER => 'сентябрь',
            self::OCTOBER => 'октябрь',
            self::NOVEMBER => 'ноябрь',
            self::DECEMBER => 'декабрь',
        ];
    }

    /**
     * @param string $intMonth
     * @return string
     */
    public static function getMonth($intMonth): string
    {
        $list = self::getList();
        return $list[$intMonth];
    }
}