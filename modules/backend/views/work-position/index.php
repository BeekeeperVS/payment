<?php

use app\components\gridView\ActionFilterColumn;
use app\components\widgets\settingPagePagination\SettingPagePagination;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\WorkPositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = translate('Work Positions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-position-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(translate('Create Work Position'), ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= SettingPagePagination::widget(['pagination' => $dataProvider->pagination])?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:100px'],
            ],
            'name',

            [
                'class' => ActionFilterColumn::class,
				'filterOptions' => [
					'class' => 'd-flex filter-actions'	
				],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'class' => 'search-actions',
                ],
            ],
        ],
    ]); ?>


</div>
