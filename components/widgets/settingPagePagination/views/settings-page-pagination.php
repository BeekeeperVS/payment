<?php

use app\components\widgets\settingPagePagination\SettingsPagePaginationAsset;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $pagination Pagination */
/* @var $this yii\web\View */


SettingsPagePaginationAsset::register($this);
?>

<!--       Блок настройки пострваничного вывода-->
<?php
$current = $pagination->getPageSize();
$values = [10, 20, 15, 25, 50];
if (!in_array($current, $values)) {
    $values[] = $current;
}
?>
<div class="wrap">
    <p>Настройка постраничного вывода</p>
    <div class="row">
        <div class="col-md-2">
            <select class="selectize-role form-control" onchange="location = this.value;">
                <?php foreach ($values as $value): ?>
                    <option value="<?= Html::encode(Url::current(['per-page' => $value, 'page' => null])) ?>"
                            <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $value ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-2">
            <input class="pagination-select form-control"
                   data-url="<?= Html::encode(Url::current(['per-page' => 1, 'page' => null])); ?>"
                   value="<?= $current ?>">
        </div>
        <div class="col-md-2">
            <button class="btn btn-primary btn-lg set-pre-page">Применить</button>
        </div>
    </div>

</div>
<!--    --------------------------------------------------------- -->
