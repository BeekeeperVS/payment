<?php


namespace app\components\factories\accrual\services\save\dto;


interface SaveAccrualDtoInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;
    /**
     * @return int
     */
    public function getUserWorkId(): int;

    /**
     * @return string
     */
    public function getAccrualType(): string;

    /**
     * @return int
     */
    public function getAccrualViewId(): int;

    /**
     * @return string
     */
    public function getComment(): string;

    /**
     * @return string
     */
    public function getPeriod(): string;

}