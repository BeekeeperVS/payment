<?php


namespace app\components\factories\subdivision\form\factory;


use app\components\factories\subdivision\exception\SubdivisionException;
use app\components\factories\subdivision\form\SubdivisionForm;

interface SubdivisionFormFactoryInterface
{
    /**
     * @param int|null $id
     * @return SubdivisionForm
     * @throws SubdivisionException
     */
    public function make(int $id = null): SubdivisionForm;
}