<?php


namespace app\components\factories\workPosition\form\factory;


use app\components\workPosition\exception\WorkPositionException;
use app\components\factories\workPosition\form\WorkPositionForm;
use app\models\db\WorkPosition;

class WorkPositionFormFactory implements WorkPositionFormFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function make(int $id = null): WorkPositionForm
    {
        return $id ? $this->findById($id) : new WorkPositionForm();
    }

    /**
     * @param int $id
     * @return WorkPositionForm
     * @throws WorkPositionException
     */
    private function findById(int $id): WorkPositionForm
    {
        $model = WorkPosition::findOne($id);
        if($model) {
            return $this->makeForm($model);
        } else {
            throw new WorkPositionException('Failed to find WorkPosition by id: ' . $id);
        }
    }

    private function makeForm(WorkPosition $model): WorkPositionForm
    {
        $modelForm = new WorkPositionForm();
        $modelForm->id = $model->id;
        $modelForm->name = $model->name;

        return  $modelForm;
    }
}