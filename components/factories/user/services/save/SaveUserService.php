<?php


namespace app\components\factories\user\services\save;


use app\components\factories\user\exception\UserException;
use app\components\factories\user\form\UserForm;
use app\components\factories\user\services\save\dto\SaveUserDtoInterface;
use app\models\db\Accrual;
use app\models\db\User;

class SaveUserService implements SaveUserServiceInterface
{

    /**
     * @inheritDoc
     */
    public function save(SaveUserDtoInterface $dto): int
    {
        $model = $dto->getId() ? $this->findUser($dto) : new User();

        $this->updateUser($dto, $model);

        return $model->id;
    }

    /**
     * @inheritDoc
     */
    public function saveAll(array $listDto)
    {
        $models = [];
        foreach ($listDto as $dto) {
            $model[$dto->getId()] = $this->findUser($dto);
        }

        $this->updateUsers($listDto, $models);

    }

    /**
     * @param SaveUserDtoInterface $dto
     * @return User
     * @throws UserException
     */
    private function findUser(SaveUserDtoInterface $dto): User
    {
        $model = User::findOne($dto->getId());
        if ($model) {
            return $model;
        } else {
            throw new UserException(SaveUserServiceInterface::class . ' Failed to find User by id: ' . $dto->getId() . "\n" . $model->getErrors());
        }
    }

    /**
     * @param SaveUserDtoInterface $dto
     * @param User $model
     * @throws UserException
     */
    private function updateUser(SaveUserDtoInterface $dto, User $model)
    {
        $model->id = $dto->getId();

        $model->name = $dto->getName();
        $model->surname = $dto->getSurName();
        $model->patronymic = $dto->getPatronymic();
        $model->work_position = $dto->getWorkPosition();
        $model->subdivision = $dto->getSubdivision();
        $model->email = $dto->getEmail();
        $model->login = $dto->getLogin();
        $model->role = $dto->getRole();
        $model->is_active = (int)$this->isActive($dto, $model);;
        $model->is_deleted = (int)$dto->getIsDeleted();

        $dtoPassword = $dto->getPassword();
        if($dtoPassword){
            $model->password = \Yii::$app->getSecurity()->generatePasswordHash($dtoPassword);
        }
        $model->first_password = $model->first_password ?? $model->password;
        $modelWorkId = $model->work_id;
        if (isset($modelWorkId) && $model->work_id != $dto->getWorkId()) {
            $accruals = $this->getOldAccruals($model->work_id);
            $model->work_id = $dto->getWorkId();
            if (!$model->save()) {
                throw new UserException("Error to save user" . "\n", $model->getErrors());
            }
            $this->updateOldAccruals($accruals, $model->work_id);

            return;
        }
        $dtoWorkId = $dto->getWorkId();
        if (isset($dtoWorkId)) {
            $model->work_id = $dto->getWorkId();
        }
        if (!$model->save()) {
            throw new UserException("Error to save user" . "\n", $model->getErrors());
        }

    }

    /**
     * @param SaveUserDtoInterface[] $listDto
     * @param User[] $models
     * @throws UserException
     */
    //реализація не перевірена (одночасне редагування декількох табельних)
    private function updateUsers(array $listDto, array $models)
    {
        $listOldAccruals = [];
        foreach ($listDto as $dto) {
            $model = $models[$dto->getId()];
            $model->id = $dto->getId();
            $model->name = $dto->getName();
            $model->surname = $dto->getSurName();
            $model->patronymic = $dto->getPatronymic();
            $model->work_position = $dto->getWorkPosition();
            $model->subdivision = $dto->getSubdivision();
            $model->email = $dto->getEmail();
            $model->login = $dto->getLogin();
            $model->password = $dto->getPassword();
            $model->role = $dto->getRole();
            $model->is_active = $this->isActive($dto, $model);
            $model->is_deleted = (int)$dto->getIsDeleted();
            $model->first_password = $model->first_password ?? $dto->getPassword();
            if (empty($model->work_id) && $model->work_id != $dto->getWorkId()) {
                $listOldAccruals[$dto->getWorkId()] = $this->getOldAccruals($model->work_id);
            }
            $model->work_id = $dto->getWorkId();
            if (!$model->save()) {
                throw new UserException("Error to save user" . "\n", $model->getErrors());
            }
        }
        foreach ($listOldAccruals as $workId => $oldAccrual) {
            $this->updateOldAccruals($oldAccrual, $workId);
        }

    }

    /**
     * @param int $oldWorkId
     * @return Accrual[]
     */
    private function getOldAccruals(int $oldWorkId): array
    {
        $accruals = Accrual::find()->where(['user_work_id' => $oldWorkId])->all();
        Accrual::deleteAll(['user_work_id' => $oldWorkId]);


        return $accruals;

    }

    /**
     * @param Accrual[] $accruals
     * @param int $workId
     */
    private function updateOldAccruals(array $accruals, int $workId)
    {
        $defaultUserWorkId = 0;
        foreach ($accruals as $accrual) {
            if (!$defaultUserWorkId) {
                $defaultUserWorkId = $accrual->default_user_work_id;
            }
            $accrualNewModel = new Accrual();
            $accrualNewModel->user_work_id = $workId;
            $accrualNewModel->default_user_work_id = $accrual->default_user_work_id;
            $accrualNewModel->accrual_type = $accrual->accrual_type;
            $accrualNewModel->accrual_view_id = $accrual->accrual_view_id;
            $accrualNewModel->period = $accrual->period;
            $accrualNewModel->sum = $accrual->sum;
            $accrualNewModel->save();
        }
        $this->replaceAccrual($workId, $defaultUserWorkId);
    }

    /**
     * @param SaveUserDtoInterface $dto
     * @param User $model
     * @return bool
     */
    private function isActive(SaveUserDtoInterface $dto, User $model): bool
    {
        $dtoWorkId = $dto->getWorkId();
        $dtoIsActive = $dto->getIsActive();
        if ($dtoWorkId) {
            if ($model->work_id) {
                return $dtoIsActive;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private function replaceAccrual(int $newWorkId, int $oldWorkId)
    {
        $modelZero = User::find()->where(['work_id' => 0])->one();
        $modelNewId = User::find()->where(['work_id' => $newWorkId])->one();
        $modelOldId = User::find()->where(['work_id' => $oldWorkId])->one();

        if (!isset($modelZero) && isset($modelNewId) && isset($modelOldId)) {
            /** @var Accrual[] $accruals */
            $accruals = Accrual::find()->where(['user_work_id' => [$newWorkId, $oldWorkId]])->all();
            foreach ($accruals as $accrual) {
                $accrual->user_work_id = $accrual->default_user_work_id;
                $accrual->save();
            }
        } elseif(!isset($modelZero)) {
            Accrual::deleteAll(['user_work_id' => $newWorkId]);
        }
    }
}