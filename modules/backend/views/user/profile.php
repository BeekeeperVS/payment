<?php

use app\components\factories\user\form\UserForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model UserForm */

$this->title = translate('Profile');
$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(translate('Update'), ['update-profile', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?= Html::button(translate('Ввести персональный ключ'), ['class' => 'btn btn-secondary show-privetKey']) ?>

    </p>

    <div class="row update-privet-block hide">
        <div class="col-md-12">
            <?= Html::textarea('privetKey', '', [
                'class' => ['form-control', 'privetKey-value'],
                'rows' => 4
            ]) ?>


        </div>
        <br>
        <div class="col-md-12">
            <?= Html::button(translate('Save'), [
                'class' => 'btn btn-secondary set-privetKey',
                'data' => [
                    'user-login' => Yii::$app->user->identity->login,
                ]
            ]) ?>
        </div>
    </div>
    <br>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

//            'work_id',
            [
                'label' => 'ФИО',
                'value' => function (UserForm $model) {
                    return $model->getFullName();
                }
            ],

            'workPosition',
            'email:email',
            'login',
//            'password',

        ],
    ]) ?>

</div>
