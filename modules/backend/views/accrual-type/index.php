<?php

use app\components\gridView\ActionFilterColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AccrualTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accrual Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accrual-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Accrual Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'alias',

            [
                'class' => ActionFilterColumn::class,
                'template' => '{update}',
            ],
        ],
    ]); ?>


</div>
