<?php

use yii\db\Migration;

/**
 * Class m190910_043446_create_table_subdivision
 */
class m190910_043446_create_table_subdivision extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%subdivision}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
            'updated_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%subdivision}}');
    }
}
