<?php
return [
    'Администратор',
    'Системный инженер',
    'Системный администратор',
    'SMM-менеджер',
    'Специалист технической поддержки',
    'Менеджер по контекстной рекламе',
    'Специалист поддержки',
    'Интернет-маркетолог',
    'Программист PHP',
    'Аналитик',
    'Контент-менеджер',
    'Тестировщик',
    'Маркетолог',
    'ИТ-специалист',
    'Front-end программист',
    'Менеджер проектов',
    'SEO-специалист',
    'Менеджер по продажам',
    'Программист 1C',
    'Дизайнер',
    'Web-программист',
    'Full stack программист',
    'Javascript-программист',
    'Менеджер по работе с клиентами',
    'Технический специалист',
    'Программист Java',
    '.Net-программист',
    'UI-дизайнер',
    'Инженер-программист',
    'Менеджер по работе с клиентами',
];
