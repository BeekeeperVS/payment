$(function () {

    var localValue = JSON.parse(localStorage.getItem('financeManagerCrypt'));
    if(!localValue){
        alert('Необходимо ввести ваш PrivetKey');
        window.location = '/backend/user/profile';
    }
    var privetKey = localValue.privetKey;
    var userLogin = 'alexandr';//localValue.userLogin;
    var applicationAlias = 'Demo'; //localValue.applicationAlias;

    readCryptoUserPage();

    function readCryptoUserPage() {

        var objectUsers = $('.accrual-decrypt');
        for (var i = 0; i <= objectUsers.length; i++) {
            var dataUserKey = $(objectUsers[i]).attr('data-user-key');
            var dataObjects = getObjectDecrypt(dataUserKey);
            var encrypt = new EncryptModel({
                'inputs': {
                    privateKey: privetKey,
                    objects: dataObjects,
                    data: '',
                    applications__alias: applicationAlias,
                    users_login: userLogin,
                    method: 'read'
                },
                cryptAes: CryptoJS,
                cryptRsa: new JSEncrypt()
            });
            var crypt = encrypt.encrypt();
            var form = [
                {name: "applications__alias", value: applicationAlias},
                {name: "users_login", value: userLogin},
                {name: "method", value: 'read'},
                {name: "data", value: crypt}
            ];
            $.ajax({
                url: 'http://encpass2.ukrtech.info/api.php',
                method: 'POST',
                data: form,
                success: function (data) {
                    var objects = JSON.parse(data).objects;
                    var sum = 0;
                    for (var i = 0; i < objects.length; i++) {
                        var counter = objects[i];
                        $('[data-crypto-key=' + counter.key_id + ']').text(JSON.parse(counter.properties).sum)
                    }
                    $('.crypto-accrual').removeClass('hide');
                }
            })

        }

    }

});

function getObjectDecrypt(selector) {
    var tagTds = $('td.' + selector);
    var objectsDecrypt = [];
    for (var i = 0; i < tagTds.length; i++) {
        var tagTd = $(tagTds[i]);
        var key_id = tagTd.data('crypto-key');
        objectsDecrypt.push({'alias': 'accrual-bortic-test', 'key_id': key_id})
    }
    return JSON.stringify(objectsDecrypt);
}

