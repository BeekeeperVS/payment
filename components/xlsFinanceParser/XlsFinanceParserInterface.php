<?php


namespace app\components\xlsFinanceParser;


interface XlsFinanceParserInterface
{

    /**
     * @param string $filePath
     * @param string $period
     * @return mixed
     */
    public static function parseXls(string $filePath, string $period);
}