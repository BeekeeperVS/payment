<?php

namespace app\components\validators;


use Yii;
use yii\validators\Validator;

class FileDateValidator extends Validator
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->message = translate('Выбраный период не совпадает c переодом у файле');
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        if (!$model->period || !$model->financeFile) {
            $model->addError($attribute, $this->message);
        }
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        return <<<JS

        function myClientValidate() {
            var period = $('.accrual-period').val();
            var input = document.getElementById('datauploadform-financefile');
            var file = input.files[0] ? input.files[0]['name'] : '';
            if (0 !== file.length && 0 !== period.length) {
                var url = $('.xls-file').attr('data-url');
                var form = $('.accrual-form form');
                formData = new FormData(form.get(0));
                $.ajax({
                    contentType: false,
                    processData: false,
                    data: formData,
                    url: url,
                    cache: false,
                    type: 'POST',
                    success: function (data) {
                        var divElement = $('.field-datauploadform-period');
                        if (!data.success) {
                            divElement.removeClass('has-success');
                            divElement.addClass('has-error');
                            $('.field-datauploadform-period .help-block').text(data.message);
                            $('.accrual-period').attr('aria-invalid', true);
                            $('button[type="submit"]').attr('disabled','disabled');
                        } else {
                            divElement.removeClass('has-error');
                            divElement.addClass('has-success');
                            $('.field-datauploadform-period .help-block').text('');
                            $('button[type="submit"]').removeAttr('disabled');
                        }
                        if(data.confirm){
                            $('.btn-accrual-upload').attr('data-confirm',data.confirm)
                        }
                    }
                })
            }
        }

        myClientValidate();
JS;

    }

}