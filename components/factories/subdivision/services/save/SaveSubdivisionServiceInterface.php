<?php


namespace app\components\factories\subdivision\services\save;


use app\components\factories\subdivision\exception\SubdivisionException;
use app\components\factories\subdivision\services\save\dto\SaveSubdivisionDtoInterface;

interface SaveSubdivisionServiceInterface
{
    /**
     * @param SaveSubdivisionDtoInterface $dto
     * @return int
     * @throws SubdivisionException
     */
    public function save(SaveSubdivisionDtoInterface $dto): int;

}