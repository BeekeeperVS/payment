<?php


namespace app\components\factories\user\view;


use app\components\AccrualTypeEnum;
use app\models\db\Accrual;
use app\models\db\AccrualView;
use app\models\db\Period;
use app\models\db\User;
use yii\data\Pagination;

class UserView implements UserViewInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int|null
     */
    public $workId;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $surName;
    /**
     * @var string
     */
    public $patronymic;
    /**
     * @var string
     */
    public $workPosition;
    /**
     * @var array
     */
    public $accruals;
    /**
     * @var array
     */
    public $sum;

    /**
     * @var Pagination
     */
    private $pagination;

    public function __construct(
        int $id,
        ?int $workId,
        string $name,
        string $surName,
        string $patronymic,
        string $workPosition,
        bool $cipher = false,
        array $filterAccrualsParams = []
    )
    {
        $this->id = $id;
        $this->workId = $workId;
        $this->name = $name;
        $this->surName = $surName;
        $this->patronymic = $patronymic;
        $this->workPosition = $workPosition;

        $this->setAccruals($cipher ? 'cipher' : 'title', $filterAccrualsParams);
    }

    /**
     * @return string[]
     */
    public static function getAccrualViewList(): array
    {
        $accruals = AccrualView::find()->where(['type_alias' => AccrualTypeEnum::ACCRUAL])->all();
        $accrualsTitleList = [];

        foreach ($accruals as $accrual) {
            $accrualsTitleList[$accrual->id] = $accrual->title;
        }

        return $accrualsTitleList;
    }


    /**
     * @inheritDoc
     */
    public function getWorkId(): int
    {
        return $this->workId;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getSurName(): string
    {
        return $this->surName;
    }

    /**
     * @inheritDoc
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @inheritDoc
     */
    public function getWorkPosition(): string
    {
        return $this->workPosition;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return "$this->surName $this->name $this->patronymic";
    }


    /**
     * @return array
     */
    public function getPeriod(): array
    {
        $periods = [];
        foreach ($this->accruals as $period => $value) {
            $periods[] = $period;
        }
        return $periods;
    }

    /**
     * @param int $arrayIndex
     * @return string
     */
    public function getPeriodByIndex(int $arrayIndex): string
    {
        $index = 0;
        foreach ($this->accruals as $period => $accruals){
            if($index != $arrayIndex){
                $index++;
                continue;
            }
            return $period;
        }
        return '';
    }

    private function setAccruals(string $cipher, array $filterAccrualsParams)
    {
        /**@var $user User */
        $user = User::findOne($this->id);
        $periods = $filterAccrualsParams['periods'] ?? Period::getList();

        foreach ($periods as $period) {
            $this->sum[$period] = [
                AccrualTypeEnum::ACCRUAL => 0,
                AccrualTypeEnum::DEDUCTION => 0,
            ];
            /**@var $accruals Accrual[] */
            $accruals = $user->getAccruals()->andWhere(['period' => $period])->all();
            if($accruals){
                foreach ($accruals as $accrual) {
                    $this->accruals[$period][$accrual->accrualView->{$cipher}][$accrual->accrualView->type_alias] =
                        "{$accrual->user_work_id}_{$period}_{$accrual->accrualView->id}";
//                    $this->sum[$period][$accrual->accrualView->type_alias] += $accrual->sum;
                }
            } else{
                unset($this->sum[$period]);
            }
//            $this->accruals[$period] = $accrualsListByPeriod;
        }

    }
}