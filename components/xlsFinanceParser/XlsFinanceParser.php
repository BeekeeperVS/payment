<?php


namespace app\components\xlsFinanceParser;


use app\components\AccrualTypeEnum;
use app\components\RoleTypeEnum;
use app\models\db\Accrual;
use app\models\db\AccrualView;
use app\models\db\Period;
use app\models\db\User;
use app\models\form\DataUploadForm;
use SimpleXLS;
use yii\rbac\Role;

class XlsFinanceParser implements XlsFinanceParserInterface
{
    /**
     * @var array
     */
    private static $accruals;

    /**
     * @var array
     */
    private static $users;

    /**
     * @param string $filePath
     * @param string $period
     * @return array|mixed
     */
    public static function parseXls(string $filePath, string $period)
    {
        $usersParse = [];
        $user_id = 0;
        $next_row = false;
        if ($xls = SimpleXLS::parse($filePath)) {

            foreach ($xls->rows() as $row) {

                // find user_id
                if (is_numeric($row[0])) {
                    $usersParse[] = $user_id = $row[0];
//                    self::deleteOldInformation($user_id, $period);
                    $next_row = true;
                    continue;
                }

                //find user name and save user
                if ($next_row) {
                    $next_row = false;
                    $userIdResult = self::addUser($user_id, $row[0]);

                    if (!$userIdResult) {
                        if (($key = array_search($user_id, $usersParse)) !== false) {
                            unset($usersParse[$key]);
                            $user_id = 0;
                        }
                    }
                    self::$users[$userIdResult] = $row[0];
                    continue;
                }

                // find user accrual
                if ($user_id) {
                    self::addAccrual($user_id, AccrualTypeEnum::ACCRUAL, $row[0], $period, (int)$row[1]);
                    self::addAccrual($user_id, AccrualTypeEnum::DEDUCTION, $row[0], $period, (int)$row[2]);
                }
            }
        } else {
            SimpleXLS::parseError();
        }
        unlink(DataUploadForm::FILE_PATH);

        $sessionAccruals = \Yii::$app->session->get('uploadInformation');
        $sessionAccruals['accruals'] = self::$accruals;
        $sessionAccruals['users'] = self::$users;
        \Yii::$app->session->set('uploadInformation', $sessionAccruals);

        return $usersParse;

    }

    /**
     * @param $filePath
     * @return mixed
     */
    public static function getTitleFile($filePath)
    {
        if ($xls = SimpleXLS::parse($filePath)) {
            $rows = $xls->rows();
            return $rows[0][0];
        }
    }

    /**
     * @param int $workId
     * @param string $userFullName
     * @return bool|mixed
     */
    private
    static function addUser(int $workId, string $userFullName)
    {
        if ($user = User::find()->where(['work_id' => $workId])->one()) {
            return $user->work_id;
        } else {
            return false;
        }
    }

    /**
     * @param string $accrualViewTitle
     * @param string $accrualTypeAlias
     * @return bool|int|mixed
     */
    private
    static function addAccrualView(string $accrualViewTitle, string $accrualTypeAlias)
    {
        if ($accrualViewTitle == 'Итого') {
            return false;
        } elseif ($accrualViewModel = AccrualView::find()->where(['title' => $accrualViewTitle, 'type_alias' => $accrualTypeAlias])->one()) {
            return $accrualViewModel->id;
        }

        $accrualViewModel = new AccrualView();
        $accrualViewModel->title = $accrualViewTitle;
        $accrualViewModel->type_alias = $accrualTypeAlias;
        if ($accrualViewModel->save()) {
            return $accrualViewModel->id;
        }
        return false;
    }

    /**
     * @param int $userWorkId
     * @param string $accrualType
     * @param string $accrualViewTitle
     * @param string $period
     * @param int $sum
     * @param string $comment
     */
    private
    static function addAccrual(int $userWorkId, string $accrualType, string $accrualViewTitle, string $period, int $sum, string $comment = '')
    {
        if ($sum && $accrualViewId = self::addAccrualView($accrualViewTitle, $accrualType)) {
            self::$accruals[$userWorkId][$accrualType][$accrualViewId] = $sum;
        } else {
            return;
        }

    }

    /**
     * @param string $period
     * @return string
     */
    private
    static function addPeriod(string $period)
    {
        if (Period::find()->where(['period' => $period])->count()) {
            return $period;
        }
        $model = new Period();
        $model->period = $period;
        $model->save();
        return $period;
    }

    /**
     * @param int $userWorkId
     * @param string $period
     */
    private
    static function deleteOldInformation(int $userWorkId, string $period)
    {
        if (Accrual::find()->where(['period' => $period, 'user_work_id' => $userWorkId])->count()) {
            $accrualModels = Accrual::find()->where([
                'period' => $period,
                'user_work_id' => $userWorkId,
            ])->all();
            foreach ($accrualModels as $accrualModel) {
                $accrualModel->delete();
            }
        }
    }
}