<?php

/* @var $this \yii\web\View */
/**@var $identity Identity */

/* @var $content string */

use app\assets\AdminAsset;

use app\components\identity\Identity;
use app\components\RoleTypeEnum;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);

$identity = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <?php $this->head() ?>
    </head>

    <body class="app sidebar-mini rtl">
    <?php $this->beginBody() ?>
    <!-- Navbar-->
    <header class="app-header">
        <a class="app-header__logo" href="<?= Url::to(['/']) ?>">Finance Manager</a>
        <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                        aria-label="Hide Sidebar"></a>
        <!-- Navbar Right Menu-->
        <ul class="app-nav">
            <li class="app-search">
                <?php if ($identity->isReplacePassword()) : ?>
                    <?= Html::a(translate('Replace password'), ['user/update-profile', 'id' => Yii::$app->user->identity->getId()], [
                        'class' => ' btn btn-danger',
                        'type' => 'button'
                    ])
                    ?>
                <?php endif; ?>
            </li>
            <li class="app-search">
                <div class="dropdown">
                    <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" id="dropdownMenu2"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Профиль ( <?= $identity->name ?>)
                    </button>

                    <div class="dropdown-menu">
                        <?= Html::a(translate('Profile'), ['user/profile'], [
                            'class' => 'dropdown-item btn btn-secondary btn-lg',
                        ]) ?>

                        <?php if (!Yii::$app->user->isGuest) : ?>
                            <div class="dropdown-divider"></div>
                            <?= Html::beginForm(['/site/logout'], 'post') ?>
                            <?= Html::submitButton(
                                translate('Выйти'),
                                ['class' => 'btn btn-secondary btn-lg dropdown-item']
                            ) ?>
                            <?= Html::endForm() ?>
                        <?php endif; ?>
                    </div>
                    <?php if ($identity->whoIs([RoleTypeEnum::ADMIN, RoleTypeEnum::FINANCE_MANAGER, RoleTypeEnum::HR_MANAGER])): ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', ['user/basket'], [
                            'class' => 'btn btn-default btn-lg',
                            'title' => 'Удаленные пользователи',
                            'type' => 'button'
                        ]) ?>
                    <?php endif; ?>
                </div>

            </li>
        </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <?= $this->render('_menu', ['identity' => $identity]); ?>
    <main class="app-content">
        <div class="container-fluid">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <div class="modal fade" id="popup" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header alert" role="alert">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p class="modal-text"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= translate('Close')?></button>
                </div>
            </div>

        </div>
    </div>

    <?php $this->endBody() ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    </body>
    </html>
<?php $this->endPage() ?>