<?php

use app\components\factories\user\form\UserForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model UserForm */
/* @var $showReplacePassword bool */

$this->title = translate('Update User') . ': ' . $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => translate('Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getFullName(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = translate('Update');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-6 col-lg-4 checkbox">
            <?= Html::label(Html::checkbox('checkbox', $showReplacePassword , [
                    'class' => 'replace-password-show'
                ]) . 'Сменить пароль') ?>
        </div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'showReplacePassword' => $showReplacePassword
    ]) ?>

</div>
