<?php

use app\components\factories\subdivision\form\factory\SubdivisionFormFactory;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model SubdivisionFormFactory */

$this->title = translate('Create Subdivision');
$this->params['breadcrumbs'][] = ['label' => translate('Subdivisions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subdivision-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
