<?php

namespace app\modules\backend\controllers;

use app\components\factories\accrual\form\factory\AccrualFormFactoryInterface;
use app\components\factories\accrual\services\save\dto\SaveAccrualDtoInterface;
use app\components\factories\accrual\services\save\SaveAccrualServiceInterface;
use app\components\factories\user\view\factory\UserViewFactoryInterface;
use app\components\RoleTypeEnum;
use app\components\view\UserAccrualView;
use app\models\form\AccrualFilterForm;
use app\models\form\PeriodFilterForm;
use Yii;
use app\models\db\Accrual;
use app\models\search\AccrualSearch;
use yii\base\Module;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccrualController implements the CRUD actions for Accrual model.
 */
class AccrualController extends Controller
{
    /**
     * @var AccrualFormFactoryInterface
     */
    private $accrualFormFactory;
    /**
     * @var SaveAccrualDtoInterface
     */
    private $saveAccrualService;
    /**
     * @var UserViewFactoryInterface
     */
    private $userViewFactory;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new NotFoundHttpException();
                },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->user->identity->whoIs([
                                RoleTypeEnum::ADMIN,
                                RoleTypeEnum::FINANCE_MANAGER,
                            ]));
                        }
                    ],
                    [
                        'actions' => ['user-page', 'filter'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ]
        ];
    }

    public function __construct(
        $id,
        Module $module,
        AccrualFormFactoryInterface $accrualFormFactory,
        SaveAccrualServiceInterface $saveAccrualService,
        UserViewFactoryInterface $userViewFactory,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->accrualFormFactory = $accrualFormFactory;
        $this->saveAccrualService = $saveAccrualService;
        $this->userViewFactory = $userViewFactory;
    }

    public function actionFilter()
    {
        $filterForm = new AccrualFilterForm();
        $periodModel = new PeriodFilterForm();
        $perPage = $_GET['per-page'] ?? null;

        $url = [];
        if (Yii::$app->request->get() && $filterForm->load(Yii::$app->request->queryParams)) {
            $url[] = 'accrual/index';
            $url['period'] = $filterForm->period;
            if($filterForm->work_id){
                $url['work-id'] = $filterForm->work_id;
            }
            if($filterForm->name){
                $url['name'] = $filterForm->name;
            }
            if($perPage){
                $url['per-page'] = $perPage;
            }
        }
        if (Yii::$app->request->get() && $periodModel->load(Yii::$app->request->queryParams)){
            $url[] = 'accrual/user-page';
            $url['period-from'] = $periodModel->period_from;
            if($periodModel->period_to){
                $url['period-to'] = $periodModel->period_to;
            }
            if($perPage){
                $url['per-page'] = $perPage;
            }
        }


        return Yii::$app->getResponse()->redirect(Url::to($url));
    }
    /**
     * @param string|null $period
     * @return string
     */
    public function actionIndex(string $period = null)
    {
        $filterForm = new AccrualFilterForm();
        if (isset($period)) {
            $filterForm->period = $period;
        }
        $filterForm->name = $_GET['name'] ?? '';
        $filterForm->work_id = $_GET['work-id'] ?? null;

        $viewsModels = $this->userViewFactory->makeAll($filterForm->name, (int)$filterForm->work_id, $filterForm->period);
        return $this->render('accountant', [
            'viewsModels' => $viewsModels['models'],
            'pagination' => $viewsModels['pagination'],
            'filterForm' => $filterForm,
        ]);
    }

    /**
     * @return string
     */
    public function actionUserPage()
    {
        $id = Yii::$app->user->identity->id;
        $periodModel = new PeriodFilterForm();
        $periodFrom = $_GET['period-from'] ?? $periodModel->period_from;
        $periodTo = $_GET['period-to'] ?? '';
        $periodModel->setPeriods($periodFrom, $periodTo);
        $periods = $periodModel->getPeriod();

        $viewModel = $this->userViewFactory->make($id, true, ['periods' => $periods]);

        $pagination = new Pagination([
            'totalCount' => count($viewModel->accruals),
            'defaultPageSize' => 10,
        ]);

        return $this->render('user_page', [
            'viewModel' => $viewModel,
            'periodModel' => $periodModel,
            'periods' => $periods,
            'pagination' => $pagination,
        ]);
    }

}
