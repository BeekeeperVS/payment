<?php
/* @var $model app\models\RestorePasswordForm */

/* @var $form yii\bootstrap\ActiveForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="modal fade" id="restorePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel"><?= translate('Restore password') ?></h4>
            </div>
            <?php $form = ActiveForm::begin([
                'method' => 'post',
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to(['/site/validate']),
//                'action' => Url::to(['/site/restore-password']),
                'id' => 'restore-form',
                'options' => ['class' => 'restore-form',],

                'fieldConfig' => [
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>
            <div class="modal-body">

                <div class="form-group">
                    <label class="control-label"><?= translate('Email / Login') ?></label>
                    <?= $form->field($model, 'email')->textInput(['autofocus' => true])->label(false) ?>
                </div>

            </div>
            <div class="modal-footer">
                <?= Html::submitButton(translate('Restore'), [
                        'class' => 'btn btn-primary btn-restore-password',
                    'name' => 'restore-button',
                    'data' => [
                        'url' => Url::to(['/site/restore-password']),
                        'url-home' => Url::to(['/site/login'])
                    ],
                ]) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>