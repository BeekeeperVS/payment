<?php


namespace app\components\factories\workPosition\form;


interface WorkPositionFormInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string
     */
    public function getName(): string;
}