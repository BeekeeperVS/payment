<?php

namespace app\commands;

use app\components\xlsFinanceParser\ParseException;
use app\models\db\Accrual;
use app\models\db\AccrualView;
use app\models\db\User;
use SimpleXLS;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ParseController extends Controller
{
    /**
     * @return int
     */
    public function actionAccrual()
    {
        $row = 1;
        if (($handle = fopen("runtime/123456.xls", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                foreach ($data as $key => $value) {
                    print_r("$key: $value |");
                }
                if ($row == 2) {
                    return;
                }
                $row++;
            }
        }
    }

    public function actionTest()
    {
        $accruals = Accrual::find()->all();
        foreach ($accruals as $accrual){
            $accrual->default_user_work_id = $accrual->user_work_id;
            $accrual->save();
        }
    }

    public function actionAccrualXls()
    {
        $user_id = '';
        $next_row = false;
        $accrualType = 'accrual';
        if ($xls = SimpleXLS::parse('runtime/123456.xls')) {
            foreach ($xls->rows() as $row) {
                // find user_id
                if (is_numeric($row[0])) {
                    $user_id = $row[0];
                    $next_row = true;
                    print_r("$user_id\n");
                    continue;
                }
                //find user name adn save user
                if ($next_row) {
                    $next_row = false;
                    $this->addUser($user_id, $row[0]);
                    continue;
                }

                // find user accrual
                if ($user_id) {
                    $accrualViewId = $this->addAccrualView($row[0], $accrualType);
                    $this->addAccrual($user_id, $accrualType, $accrualViewId, '2019-07-18 00:00:00', $row[1]);
                    print_r("$row[0]: $row[1]\n");
                }
            }
        } else {
            echo SimpleXLS::parseError();
        }

    }

    private function addUser(int $workId, string $userFullName)
    {
        if ($user = User::find()->where(['work_id' => $workId])->one()) {
            return $user->work_id;
        }
        $user = new User();
        $userFullName = explode(' ', $userFullName);
        $user->work_id = $workId;
        $user->name = $userFullName[0];
        $user->surname = $userFullName[1];
        $user->patronymic = $userFullName[2];

        if ($user->save()) {
            return $user->work_id;
        }
        return new ParseException('False to save User');
    }

    private function addAccrualView(string $accrualViewTitle, string $accrualTypeAlias)
    {
        if ($accrualViewTitle == 'Итого') {
            return false;
        } elseif ($accrualViewModel = AccrualView::find()->where(['title' => $accrualViewTitle])->one()) {
            return $accrualViewModel->id;
        }

        $accrualViewModel = new AccrualView();
        $accrualViewModel->title = $accrualViewTitle;
        $accrualViewModel->type_alias = $accrualTypeAlias;
        if ($accrualViewModel->save()) {
            return $accrualViewModel->id;
        }
        return false;
    }

    private function addAccrual(int $userWorkId, string $accrualType, int $accrualView, string $period, int $sum, string $comment = '')
    {
        $accrualModel = new Accrual();
        $accrualModel->user_work_id = $userWorkId;
        $accrualModel->accrual_type = $accrualType;
        $accrualModel->accrual_view_id = $accrualView; //змінити на accrual_view_id!!!
        $accrualModel->period = $period;
        $accrualModel->sum = $sum;
        print_r($accrualModel->save());
    }
}
