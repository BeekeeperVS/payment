<?php


namespace app\assets;


use yii\web\AssetBundle;

class CreateUpdateCryptoAsset extends AssetBundle
{
    public $basePath = '@webroot/';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $css = [

    ];
    public $js = [
        'asset-manager/crypto/js/createUpdate.js',
    ];
    public $depends = [
       CryptoAsset::class,
    ];
}