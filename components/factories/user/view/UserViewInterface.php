<?php


namespace app\components\factories\user\view;


interface UserViewInterface
{

    /**
     * @return int
     */
    public function getWorkId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getSurName(): string;

    /**
     * @return string
     */
    public function getPatronymic(): string;

    /**
     * @return string
     */
    public function getWorkPosition(): string;

}