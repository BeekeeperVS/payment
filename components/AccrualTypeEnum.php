<?php


namespace app\components;


abstract class AccrualTypeEnum
{
    const ACCRUAL = 'accrual';
    const DEDUCTION = 'deduction';

    public static function getAccrualTypeList()
    {
        return [
            self::ACCRUAL => 'Начислено',
            self::DEDUCTION => 'Удержано',
        ];
    }

    /**
     * @param string $accrualName
     * @return mixed
     */
    public static function getAccrualType(string $accrualName)
    {
        $accrualList =  self::getAccrualTypeList();

        foreach ($accrualList as $key => $value){
            if($accrualList[$key] == $accrualName){
                return $accrualList[$key];
            }
        }
    }
}