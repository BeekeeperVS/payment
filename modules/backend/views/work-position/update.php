<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\WorkPosition */

$this->title = translate('Update Work Position').': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => translate('Work Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = translate('Update');
?>
<div class="work-position-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
