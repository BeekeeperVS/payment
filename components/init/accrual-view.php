<?php
return [
    [
        'title' => 'Оклад по дням (ставка)',
        'cipher' => 'Основное добро',
        'type_alias' => 'accrual',
    ],
    [
        'title' => 'Оплата по заданиям / овертаймы',
        'cipher' => 'Дополнительное добро',
        'type_alias' => 'accrual',
    ],
    [
        'title' => 'Премия по итогам месяца',
        'cipher' => 'Добытое добро',
        'type_alias' => 'accrual',
    ],
    [
        'title' => 'Премия с Zendo',
        'cipher' => 'Полученное добро',
        'type_alias' => 'accrual',
    ],
    [
        'title' => 'Сдельная оплата (Внутренние проекты/задачи компании)',
        'cipher' => 'Нажитое добро',
        'type_alias' => 'accrual',
    ],
    [
        'title' => 'Сдельная оплата (тариф/ставка) ПРОЕКТЫ',
        'cipher' => 'Отжатое добро',
        'type_alias' => 'accrual',
    ],
    [
        'title' => 'Отпускные',
        'cipher' => 'Возвращенное добро',
        'type_alias' => 'accrual',
    ],
    [
        'title' => 'Процент от продаж по отв.',
        'cipher' => 'Мини-добро',
        'type_alias' => 'accrual',
    ],
];