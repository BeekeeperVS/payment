<?php

use yii\db\Migration;

/**
 * Class m190718_103033_create_table_accrual
 */
class m190718_103033_create_table_accrual extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%accrual}}', [
            'id' => $this->primaryKey(),
            'user_work_id' => $this->integer()->notNull(),
            'accrual_type' => $this->string(100)->notNull(),
            'accrual_view_id' => $this->integer()->notNull(),
            'comment' => $this->string(255)->null(),
            'period' => $this->string()->notNull(),
            'sum' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
            'updated_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
        ], $tableOptions);

        $this->createIndex('idx-accrual-user_work_id', '{{%accrual}}', 'user_work_id');
        $this->createIndex('idx-accrual-accrual_type', '{{%accrual}}', 'accrual_type');
        $this->createIndex('idx-accrual-accrual_view_id', '{{%accrual}}', 'accrual_view_id');
        $this->createIndex('idx-accrual-period', '{{%accrual}}', 'period');
        $this->createIndex('idx-accrual-sum', '{{%accrual}}', 'sum');

        $this->addForeignKey(
            'fk-accrual-user_work_id',
            '{{%accrual}}',
            'user_work_id',
            '{{%user}}',
            'work_id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accrual-accrual_type',
            '{{%accrual}}',
            'accrual_type',
            '{{%accrual_type}}',
            'alias',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accrual-accrual_view_id',
            '{{%accrual}}',
            'accrual_view_id',
            '{{%accrual_view}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%accrual}}');
    }

}
