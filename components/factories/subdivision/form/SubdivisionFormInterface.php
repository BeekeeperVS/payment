<?php


namespace app\components\factories\subdivision\form;


interface SubdivisionFormInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string
     */
    public function getName(): string;
}