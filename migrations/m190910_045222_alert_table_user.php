<?php

use yii\db\Migration;

/**
 * Class m190910_045222_alert_table_user
 */
class m190910_045222_alert_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \app\models\db\User::deleteAll();

        $this->dropIndex('idx-user-work_position','{{%user}}');

        $this->alterColumn('{{%user}}','work_position', $this->integer()->null());
        $this->addColumn('{{%user}}', 'subdivision', $this->integer());

        $this->createIndex('idx-user-work_position', '{{%user}}', 'work_position');
        $this->createIndex('idx-user-subdivision', '{{%user}}', 'subdivision');

        $this->addForeignKey(
            'fk-accrual-user_work_position',
            '{{%user}}',
            'work_position',
            '{{%work_position}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-accrual-user_subdivision',
            '{{%user}}',
            'subdivision',
            '{{%subdivision}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-user-work_position','{{%user}}');
        $this->dropForeignKey('fk-accrual-user_work_position','{{%user}}');
        $this->alterColumn('{{%user}}','work_position',$this->string(100)->null());
        $this->createIndex('idx-user-work_position', '{{%user}}', 'work_position');

        $this->dropColumn('{{%user}}', 'subdivision');

    }

}
