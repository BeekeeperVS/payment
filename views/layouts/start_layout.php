<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AuthorizationAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AuthorizationAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
    <?= $content ?>
</section>

<div class="modal fade" id="popup" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= translate('Востановление пароля')?></h4>
            </div>
            <div class="modal-body">
                <?php if ($sendPassword = Yii::$app->session->getFlash('sendPassword')): ?>
                    <?= Html::tag('div', $sendPassword['message'], [
                        'class' => ['alert', 'alert-' . $sendPassword['class']],
                        'role' => 'alert'
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= translate('Close')?></button>
            </div>
        </div>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
