<?php

use yii\db\Migration;

/**
 * Class m190924_064006_alert_column_is_active_table_user
 */
class m190924_064006_alert_column_is_active_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%user}}',
            'is_active',
            $this->integer()->notNull()->defaultValue(1)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%user}}',
            'is_active',
            $this->integer()->notNull()->defaultValue(0)
        );
    }

}
