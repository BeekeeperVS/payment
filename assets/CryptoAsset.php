<?php


namespace app\assets;


use yii\web\AssetBundle;

class CryptoAsset extends AssetBundle
{
    public $basePath = '@webroot/';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $css = [

    ];
    public $js = [
        '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
        'asset-manager/admin/js/cookie.js',
        'asset-manager/crypto/js/jsencrypt.js',
        'asset-manager/crypto/js/CryptoJS-v3.1.2/rollups/aes.js',
        'asset-manager/crypto/js/CryptoJS-v3.1.2/rollups/sha256.js',
        'asset-manager/crypto/js/CryptoJS-v3.1.2/rollups/pbkdf2.js',
        'asset-manager/crypto/js/CryptoJS-v3.1.2/components/enc-base64.js',
        'asset-manager/crypto/js/encrypt.js',
    ];

}