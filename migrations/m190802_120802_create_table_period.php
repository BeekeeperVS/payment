<?php

use yii\db\Migration;

/**
 * Class m190802_120802_create_table_period
 */
class m190802_120802_create_table_period extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%period}}', [
            'id' => $this->primaryKey(),
            'period' => $this->string(100)->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
            'updated_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%period}}');
    }

}
