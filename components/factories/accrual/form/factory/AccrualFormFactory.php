<?php


namespace app\components\factories\accrual\form\factory;


use app\components\factories\accrual\exception\AccrualException;
use app\components\factories\accrual\form\AccrualForm;
use app\models\db\Accrual;

class AccrualFormFactory implements AccrualFormFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function make(int $id = null): AccrualForm
    {
        return $id ? $this->findAccrual($id) : new AccrualForm();
    }

    /**
     * @inheritDoc
     */
    public function makeFromUser(int $userWorkId, string $period): array
    {
        return $this->findAccrualsByUser($userWorkId, $period);
    }

    /**
     * @inheritDoc
     */
    public function makeByParameters(int $userWorkId, string $period, int $accrualViewId, string $accrualType): AccrualForm
    {
        return $this->findAccrualsByParameters($userWorkId, $period, $accrualViewId, $accrualType);
    }

    /**
     * @param int $id
     * @return AccrualForm
     * @throws AccrualException
     */
    private function findAccrual(int $id): AccrualForm
    {
        $model = Accrual::findOne($id);

        if ($model) {
            $accrualFormModel = $this->makeAccrualForm($model);
            return $accrualFormModel;
        } else {
            throw new AccrualException('Failed to find Accrual by id: ' . $id);
        }
    }

    /**
     * @param int|null $userWorkId
     * @param string|null $period
     * @return AccrualForm[]
     */
    private function findAccrualsByUser(int $userWorkId, string $period): array
    {
        $models = Accrual::find()->where([
            'user_work_id' => $userWorkId,
            'period' => $period,
        ])->all();

        $accrualFormModels = [];
        foreach ($models as $model) {
            $accrualFormModels[] = $this->makeAccrualForm($model);
        }

        return $accrualFormModels;
    }

    /**
     * @param int $userWorkId
     * @param string $period
     * @param int $accrualViewId
     * @param string $accrualType
     * @return AccrualForm
     */
    private function findAccrualsByParameters(int $userWorkId, string $period, int $accrualViewId, string $accrualType): AccrualForm
    {
        $model = Accrual::find()->where([
            'user_work_id' => $userWorkId,
            'period' => $period,
            'accrual_view_id' => $accrualViewId,
            'accrual_type' => $accrualType
        ])->one();

        if ($model) {
            $accrualFormModel = $this->makeAccrualForm($model);
            return $accrualFormModel;
        } else {
            $model = new AccrualForm();
            $model->userWorkId = $userWorkId;
            $model->period = $period;
            $model->accrualViewId = $accrualViewId;
            $model->accrualType = $accrualType;
            return $model;
        }

    }

    /**
     * @param Accrual $model
     * @return AccrualForm
     */
    private function makeAccrualForm(Accrual $model): AccrualForm
    {
        $accrualFormModel = new AccrualForm();

        $accrualFormModel->id = $model->id;
        $accrualFormModel->userWorkId = $model->user_work_id;
        $accrualFormModel->accrualType = $model->accrual_type;
        $accrualFormModel->accrualViewId = $model->accrual_view_id;
        $accrualFormModel->comment = $model->comment;

        return $accrualFormModel;
    }
}