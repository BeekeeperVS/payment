<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\AccrualType */

$this->title = 'Create Accrual Type';
$this->params['breadcrumbs'][] = ['label' => 'Accrual Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accrual-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
