<?php

namespace app\modules\backend\controllers;

use app\components\factories\workPosition\form\factory\WorkPositionFormFactoryInterface;
use app\components\factories\workPosition\services\save\SaveWorkPositionServiceInterface;
use app\components\RoleTypeEnum;
use app\components\workPosition\exception\WorkPositionException;
use Yii;
use app\models\db\WorkPosition;
use app\models\search\WorkPositionSearch;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * WorkPositionController implements the CRUD actions for WorkPosition model.
 */
class WorkPositionController extends Controller
{
    /**
     * @var WorkPositionFormFactoryInterface
     */
    private $workPositionFormFactory;
    /**
     * @var SaveWorkPositionServiceInterface
     */
    private $saveWorkPositionService;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new NotFoundHttpException();
                },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'create', 'update', 'delete',
                        ],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->user->identity->whoIs([
                                RoleTypeEnum::ADMIN,
                                RoleTypeEnum::FINANCE_MANAGER,
                                RoleTypeEnum::HR_MANAGER
                            ]));
                        }
                    ],
                ]
            ]
        ];
    }

    public function __construct(
        $id,
        Module $module,
        WorkPositionFormFactoryInterface $workPositionFormFactory,
        SaveWorkPositionServiceInterface $saveWorkPositionService,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->workPositionFormFactory = $workPositionFormFactory;
        $this->saveWorkPositionService = $saveWorkPositionService;
    }

    /**
     * Lists all WorkPosition models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkPositionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new WorkPosition model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     * @throws WorkPositionException
     */
    public function actionCreate()
    {
        $model = $this->workPositionFormFactory->make();

        if ($model->load(Yii::$app->request->post())) {
            $this->saveWorkPositionService->save($model->makeDto());
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WorkPosition model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @return string|Response
     * @throws WorkPositionException
     */
    public function actionUpdate($id)
    {
        $model = $this->workPositionFormFactory->make($id);

        if ($model->load(Yii::$app->request->post())) {
            $this->saveWorkPositionService->save($model->makeDto());
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WorkPosition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the WorkPosition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorkPosition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WorkPosition::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
