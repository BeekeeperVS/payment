<?php


namespace app\components\factories\user\form;


interface UserFormInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return int
     */
    public function getWorkId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getSurName(): string;

    /**
     * @return string
     */
    public function getPatronymic(): string;

    /**
     * @return int|null
     */
    public function getWorkPosition(): ?int;

    /**
     * @return int|null
     */
    public function getSubdivision(): ?int;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getLogin(): string;

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @return int
     */
    public function getRole(): int;

    /**
     * @return bool
     */
    public function getIsActive(): bool;

    /**
     * @return bool
     */
    public function getIsDeleted(): bool;

}