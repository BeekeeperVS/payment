<?php
/**
 * Created by PhpStorm.
 * User: beeke
 * Date: 13.02.2019
 * Time: 12:21
 */

namespace app\components\validators;


use Yii;
use yii\validators\EmailValidator;
use yii\validators\StringValidator;
use yii\validators\Validator;

class LoginOrEmailValidator extends Validator
{
    /**
     * @var Validator
     */
    protected $emailValidator;

    /**
     * @var Validator
     */
    protected $loginValidator;

    public function init()
    {
        parent::init();
        $this->message = translate('Необходимо заполнить E-mail/Login');
    }

    public function validateAttribute($model, $attribute)
    {
        if ($this->getEmailValidator()->validate($model->$attribute)) {
        } else {
            $model->login = $model->$attribute;
            $model->email = '';
        }
    }

//    public function clientValidateAttribute($model, $attribute, $view)
//    {
//        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
//        $emailPattern = $this->getEmailValidator()->pattern;
//        $loginValidator = $this->getStringValidator();
//        $loginValidator->clientValidateAttribute($model, $attribute, $view);
//        return <<<JS
//
//        function validateEmail(email)
//        {
//            var re = $emailPattern;
//            return re.test(String(email).toLowerCase());
//        }
//
//        function myClientValidate()
//        {
//            if (!validateEmail(value)){
//                    messages.push('$message');
//            }
//        }
//
//        myClientValidate();
//JS;
//
//    }

    /**
     * @return Validator
     */
    protected function getEmailValidator(): Validator
    {
        if ($this->emailValidator === null) {
            $this->emailValidator = new EmailValidator();
        }
        return $this->emailValidator;
    }

    /**
     * @return Validator
     */
    protected function getStringValidator(): Validator
    {
        if ($this->loginValidator === null) {
            $this->loginValidator = new StringValidator();
        }
        return $this->loginValidator;
    }

}