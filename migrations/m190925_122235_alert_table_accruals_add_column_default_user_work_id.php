<?php

use yii\db\Migration;

/**
 * Class m190925_122235_alert_table_accruals_add_column_default_user_work_id
 */
class m190925_122235_alert_table_accruals_add_column_default_user_work_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%accrual}}',
            'default_user_work_id',
            $this->integer()->null()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%accrual}}', 'default_user_work_id');
    }


}
