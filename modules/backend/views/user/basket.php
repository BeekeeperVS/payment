<?php

use app\models\db\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = translate('Basket');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode(translate("Deleted Users")) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'work_id',
            'name',
            'surname',
            'patronymic',
            'work_position',


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{restore} {delete}',
                'buttons' => [
                    'restore' => function ($url, User $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-plus-sign"></span>',
                            ['/backend/user/restore', 'id' => $model->id],
                            [
                                'class' => ['action-button'],
                                'title' => translate('Restore'),

                            ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
