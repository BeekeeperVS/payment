<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "accrual".
 *
 * @property int $id
 * @property int $user_work_id
 * @property string $accrual_type
 * @property int $accrual_view_id
 * @property string $comment
 * @property string $period

 * @property string $created_at
 * @property string $updated_at
 * @property int $default_user_work_id
 *
 * @property AccrualType $accrualType
 * @property AccrualView $accrualView
 * @property User $userWork
 */
class Accrual extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accrual';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_work_id', 'accrual_type', 'accrual_view_id', 'period'], 'required'],
            [['user_work_id', 'accrual_view_id', 'default_user_work_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['accrual_type'], 'string', 'max' => 100],
            [['comment', 'period'], 'string', 'max' => 255],
            [['accrual_type'], 'exist', 'skipOnError' => true, 'targetClass' => AccrualType::className(), 'targetAttribute' => ['accrual_type' => 'alias']],
            [['accrual_view_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccrualView::className(), 'targetAttribute' => ['accrual_view_id' => 'id']],
            [['user_work_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_work_id' => 'work_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_work_id' => 'User Work ID',
            'accrual_type' => 'Accrual Type',
            'accrual_view_id' => 'Accrual View ID',
            'comment' => 'Comment',
            'period' => 'Period',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'default_user_work_id' => 'Default User Work ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccrualType()
    {
        return $this->hasOne(AccrualType::className(), ['alias' => 'accrual_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccrualView()
    {
        return $this->hasOne(AccrualView::className(), ['id' => 'accrual_view_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['work_id' => 'user_work_id']);
    }
}
