<?php


namespace app\components\factories\subdivision\form\factory;


use app\components\factories\accrual\form\AccrualForm;
use app\components\factories\subdivision\exception\SubdivisionException;
use app\components\factories\subdivision\form\SubdivisionForm;
use app\models\db\Subdivision;

class SubdivisionFormFactory implements SubdivisionFormFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function make(int $id = null): SubdivisionForm
    {
        return $id ? $this->findById($id) : new SubdivisionForm();
    }


    /**
     * @param int $id
     * @return SubdivisionForm
     * @throws SubdivisionException
     */
    private function findById(int $id): SubdivisionForm
    {
        /** @var $model Subdivision */
        $model = Subdivision::findOne($id);
        if ($model) {
            return $this->makeForm($model);
        } else {
            throw new SubdivisionException('False to find Subdivision by id: ' . $id);
        }
    }

    /**
     * @param Subdivision $model
     * @return SubdivisionForm
     */
    private function makeForm(Subdivision $model): SubdivisionForm
    {
        $modelForm = new SubdivisionForm();
        $modelForm->id = $model->id;
        $modelForm->name = $model->name;

        return $modelForm;
    }
}