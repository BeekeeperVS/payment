<?php


namespace app\commands;


use app\models\db\User;
use yii\console\Controller;

class UserController extends Controller
{

    public function actionHashPassword()
    {
        $users = User::find()->all();
        foreach ($users as $user) {
            $user->password = $user->first_password = \Yii::$app->getSecurity()->generatePasswordHash($user->password);
            $user->save();
        }
    }

}