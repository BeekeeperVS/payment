<?php


use app\components\identity\Identity;
use yii\bootstrap\BootstrapAsset;
use yii\swiftmailer\Mailer;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'container' => require __DIR__ . '/_container.php',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'ru-RU',
    'layout' => 'start_layout',
    'defaultRoute' => 'site/login',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gQhhJRyy1pBjw1KLDxWJdr_g3n0tKQMx',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => Identity::class,
//            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => Mailer::class,
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'bortik.work@gmail.com',
                'password' => 'bortik123456789',
                'authMode'=>  'login',
                'port' => '587',
                'encryption' => 'tls',
            ],


            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                BootstrapAsset::class => [
                    'css' => [
                        'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
                        'https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                    ],
                    'js' => [
                        'https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'
                    ],
                ],
                'kartik\form\ActiveFormAsset' => [
                    'bsDependencyEnabled' => false // do not load bootstrap assets for a specific asset bundle
                ],
            ],
        ],
        'db' => $db,

        'i18n' => [
            'translations' => [
                'translate' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/components/translate',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => array(
                'backend/user/update/id=<id:\d+>' => 'backend/user/update',
                'backend/accrual-view/update/id=<id:\d+>' => 'backend/accrual-view/update',
                'backend/accrual/page=<page:\d+>' =>  '/backend/accrual/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),

        ],

    ],
    'modules' => [
        'backend' => [
            'class' => \app\modules\backend\Module::class,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
