<?php


namespace app\components\factories\user\services\save;


use app\components\factories\user\services\save\dto\SaveUserDtoInterface;

interface SaveUserServiceInterface
{
    /**
     * @param SaveUserDtoInterface $saveUserDto
     * @return int
     */
    public function save(SaveUserDtoInterface $saveUserDto): int;

    /**
     * @param SaveUserDtoInterface[] $listDto
     */
    public function saveAll(array $listDto); //нада доопрацювати реалізацію та перевіри працездатністю методу
}