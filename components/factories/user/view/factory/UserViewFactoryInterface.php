<?php


namespace app\components\factories\user\view\factory;


use app\components\factories\user\view\UserView;

interface UserViewFactoryInterface
{
    /**
     * @param int $id
     * @param bool $cipher
     * @param array $filterAccrualsParams
     * @return UserView
     */
    public function make(int $id, bool $cipher = false, array $filterAccrualsParams = []): UserView;

    /**
     * @param int[] $usersId
     * @return UserView[]
     */
    public function makeByUsersId(array $usersId): array;

    /**
     * @param string $period
     * @param string|null $name
     * @param int|null $user_work_id
     * @return UserView[]
     */
    public function makeAll(string $name = null, int $user_work_id = null, $period = null): array;
}