<?php


namespace app\components\factories\workPosition\services\save;

use app\components\factories\workPosition\services\save\dto\SaveWorkPositionDtoInterface;
use app\components\workPosition\exception\WorkPositionException;

interface SaveWorkPositionServiceInterface
{
    /**
     * @param SaveWorkPositionDtoInterface $dto
     * @return int
     * @throws WorkPositionException
     */
    public function save(SaveWorkPositionDtoInterface $dto): int;
}