<?php

use app\components\identity\Identity;
use app\components\RoleTypeEnum;
use yii\helpers\Url;

/**@var $identity Identity */

?>
<aside class="app-sidebar">
    <ul class="app-menu">

        <li class="treeview"><a class="app-menu__item" href="<?= Url::to(['/backend/accrual/user-page']) ?>">
                <i class="app-menu__icon fa fa-th-list"></i>
                <span class="app-menu__label"><?= translate('User Page') ?></span>
            </a>
        </li>
        <?php if ($identity->whoIs([
            RoleTypeEnum::ADMIN,
            RoleTypeEnum::FINANCE_MANAGER,
        ])) : ?>
            <li class="treeview">
                <a class="app-menu__item" href="<?= Url::to(['/backend/accrual-view']) ?>">
                    <i class="app-menu__icon fa fa-th-list"></i>
                    <span class="app-menu__label"><?= translate('Accrual Views') ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if ($identity->whoIs([
            RoleTypeEnum::ADMIN,
            RoleTypeEnum::FINANCE_MANAGER,
        ])) : ?>
            <li class="treeview"><a class="app-menu__item" href="<?= Url::to(['/backend/accrual']) ?>">
                    <i class="app-menu__icon fa fa-th-list"></i>
                    <span class="app-menu__label"><?= translate('Accruals') ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if ($identity->whoIs([
            RoleTypeEnum::ADMIN,
            RoleTypeEnum::FINANCE_MANAGER,
            RoleTypeEnum::HR_MANAGER
        ])) : ?>
            <li class="treeview"><a class="app-menu__item" href="<?= Url::to(['/backend/user']) ?>">
                    <i class="app-menu__icon fa fa-th-list"></i>
                    <span class="app-menu__label"><?= translate('Users') ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if ($identity->whoIs([
            RoleTypeEnum::FINANCE_MANAGER,
        ])) : ?>
            <li class="treeview"><a class="app-menu__item" href="<?= Url::to(['/backend/upload-accrual/upload']) ?>">
                    <i class="app-menu__icon fa fa-th-list"></i>
                    <span class="app-menu__label"><?= translate('Upload 1С file') ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if ($identity->whoIs([
            RoleTypeEnum::ADMIN,
            RoleTypeEnum::FINANCE_MANAGER,
            RoleTypeEnum::HR_MANAGER
        ])) : ?>
            <li class="treeview">
                <a class="app-menu__item" href="#" data-toggle="treeview">
                    <i class="app-menu__icon fa fa-th-list"></i><span
                            class="app-menu__label"><?= translate('HandBook') ?></span>
                    <i class="treeview-indicator fa fa-angle-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a class="treeview-item" href="<?= Url::to(['/backend/work-position/index']) ?>">
                            <i class="icon fa fa-circle-o"></i>
                            <?= translate('Work Positions') ?>
                        </a>
                    </li>
                    <li>
                        <a class="treeview-item" href="<?= Url::to(['/backend/subdivision/index']) ?>">
                            <i class="icon fa fa-circle-o"></i>
                            <?= translate('Subdivisions') ?>
                        </a>
                    </li>

                </ul>
            </li>
        <?php endif; ?>
    </ul>
</aside>