<?php

use yii\db\Migration;

/**
 * Class m190716_074019_create_table_accrual_view
 */
class m190716_074019_create_table_accrual_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%accrual_view}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'cipher' => $this->string(100)->null(),
            'type_alias' => $this->string(100)->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
            'updated_at' => $this->dateTime()->notNull()->defaultValue(date('Y-m-d')),
        ], $tableOptions);

        $this->createIndex('idx-accrual_view-title', '{{%accrual_view}}', 'title');
        $this->createIndex('idx-accrual_view-type_alias', '{{%accrual_view}}', 'type_alias');
        $this->createIndex('idx-accrual_view-cipher', '{{%accrual_view}}', 'cipher');

        $this->addForeignKey(
            'fk-book_tag-type_alias',
            '{{%accrual_view}}',
            'type_alias',
            '{{%accrual_type}}',
            'alias',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%accrual_view}}');
    }
}
