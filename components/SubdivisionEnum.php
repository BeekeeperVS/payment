<?php


namespace app\components;


use app\models\db\Subdivision;

abstract class SubdivisionEnum
{
    public static function getSubdivisionList()
    {
        $list = Subdivision::find()->all();

        foreach ($list as $subdivision){
            $result[$subdivision->id] = $subdivision->name;
        }

        return $result;
    }
}