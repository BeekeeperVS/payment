<?php


namespace app\components\factories\accrual\services\save;


use app\components\factories\accrual\exception\AccrualException;
use app\components\factories\accrual\services\save\dto\SaveAccrualDtoInterface;

interface SaveAccrualServiceInterface
{
    /**
     * @param SaveAccrualDtoInterface $dto
     * @return int
     * @throws AccrualException
     */
    public function save(SaveAccrualDtoInterface $dto): int;
}