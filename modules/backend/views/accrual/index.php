<?php

use app\components\gridView\ActionFilterColumn;
use app\models\db\Accrual;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AccrualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accruals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accrual-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Accrual', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
          'class' => 'table-responsive',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'attribute' => 'user_work_id',
                'value' => function (Accrual $model) {
                    return $model->user->surname . ' ' . $model->user->name;
                }
            ],
            'accrual_type',
            [
                'attribute' => 'accrual_view_id',
                'value' => function (Accrual $model) {
                    return $model->accrualView->title;
                }
            ],
//            'comment',
            'period',
            'sum',

            [
                'class' => ActionFilterColumn::class,
            ],
        ],
    ]); ?>


</div>
