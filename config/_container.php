<?php

use app\components\factories\accrual\form\factory\AccrualFormFactory;
use app\components\factories\accrual\form\factory\AccrualFormFactoryInterface;
use app\components\factories\accrual\services\save\SaveAccrualService;
use app\components\factories\accrual\services\save\SaveAccrualServiceInterface;
use app\components\factories\subdivision\form\factory\SubdivisionFormFactory;
use app\components\factories\subdivision\form\factory\SubdivisionFormFactoryInterface;
use app\components\factories\subdivision\services\save\SaveSubdivisionService;
use app\components\factories\subdivision\services\save\SaveSubdivisionServiceInterface;
use app\components\factories\user\form\factory\UserFormFactory;
use app\components\factories\user\form\factory\UserFormFactoryInterface;
use app\components\factories\user\services\save\SaveUserService;
use app\components\factories\user\services\save\SaveUserServiceInterface;
use app\components\factories\user\view\factory\UserViewFactory;
use app\components\factories\user\view\factory\UserViewFactoryInterface;
use app\components\factories\workPosition\form\factory\WorkPositionFormFactory;
use app\components\factories\workPosition\form\factory\WorkPositionFormFactoryInterface;
use app\components\factories\workPosition\services\save\SaveWorkPositionService;
use app\components\factories\workPosition\services\save\SaveWorkPositionServiceInterface;


return [
    'singletons' => [

        //Accrual
        AccrualFormFactoryInterface::class => AccrualFormFactory::class,
        SaveAccrualServiceInterface::class => SaveAccrualService::class,

        //User
        UserFormFactoryInterface::class => UserFormFactory::class,
        SaveUserServiceInterface::class => SaveUserService::class,
        UserViewFactoryInterface::class => UserViewFactory::class,

        //Subdivision
        SubdivisionFormFactoryInterface::class => SubdivisionFormFactory::class,
        SaveSubdivisionServiceInterface::class => SaveSubdivisionService::class,

        //WorkPosition
        WorkPositionFormFactoryInterface::class => WorkPositionFormFactory::class,
        SaveWorkPositionServiceInterface::class => SaveWorkPositionService::class,

    ]
];