<?php


namespace app\modules\backend\controllers;

use app\components\AccrualTypeEnum;
use app\components\factories\accrual\exception\AccrualException;
use app\components\factories\accrual\form\AccrualForm;
use app\components\factories\accrual\form\factory\AccrualFormFactoryInterface;
use app\components\factories\accrual\services\save\SaveAccrualServiceInterface;
use app\components\factories\user\view\factory\UserViewFactoryInterface;
use app\components\RoleTypeEnum;
use app\components\view\UserAccrualView;
use app\models\db\Accrual;
use app\models\db\AccrualView;
use app\models\db\Period;
use app\models\db\User;
use app\models\form\DataUploadForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class UploadAccrualController extends Controller
{
    /**
     * @var UserViewFactoryInterface
     */
    private $userViewFactory;
    /**
     * @var AccrualFormFactoryInterface
     */
    private $accrualFormFactory;
    /**
     * @var SaveAccrualServiceInterface
     */
    private $saveAccrualService;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new NotFoundHttpException();
                },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['upload', 'view', 'edit-accrual', 'save', 'validate-file'],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->user->identity->whoIs([RoleTypeEnum::FINANCE_MANAGER]));
                        }
                    ],
                ]
            ]
        ];
    }

    public function __construct(

        $id,
        Module $module,
        AccrualFormFactoryInterface $accrualFormFactory,
        SaveAccrualServiceInterface $saveAccrualService,
        UserViewFactoryInterface $userViewFactory,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->userViewFactory = $userViewFactory;
        $this->accrualFormFactory = $accrualFormFactory;
        $this->saveAccrualService = $saveAccrualService;
    }

    /**
     * Lists all Accrual models.
     * @return mixed
     */
    public function actionUpload()
    {

        $model = new DataUploadForm();
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->financeFile = UploadedFile::getInstance($model, 'financeFile');
            Yii::$app->session->set('uploadInformation', ['period' => $model->period]);
            if ($users = $model->save()) {
                return $this->redirect(['view', 'period' => $model->period, 'users' => $users]);
            }
        }
        return $this->render('_form', ['model' => $model]);
    }

    /**
     * @param string $period
     * @param array $users
     * @return string
     */
    public function actionView(string $period, array $users = [])
    {

        $viewsModels = $this->userViewFactory->makeByUsersId($users);

        return $this->render('preview', [
            'period' => $period,
            'viewsModels' => $viewsModels,
        ]);
    }

    public function actionEditAccrual()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $userId = $_POST['userId'];
        $accrualId = $_POST['accrualId'];
        $value = $_POST['value'];

        $sessionModels = Yii::$app->session->get('uploadInformation');
        $sessionModels['accruals'][$userId][AccrualTypeEnum::ACCRUAL][$accrualId] = $value;
        Yii::$app->session->set('uploadInformation', $sessionModels);

        $accrualSum = array_sum($sessionModels['accruals'][$userId][AccrualTypeEnum::ACCRUAL]);

        return $accrualSum;
    }

    /** save accrual from session */
    public function actionSave()
    {
        $sessionModels = Yii::$app->session->get('uploadInformation');
        $period = $this->addPeriod($sessionModels['period']);
        $users = $sessionModels['users'];
        foreach ($users as $userId => $fullName) {
            Accrual::deleteAll([
                'user_work_id' => $userId,
                'period' => $period
            ]);

        }
        foreach ($sessionModels['accruals'] as $userId => $accruals) {
            if (isset($accruals[AccrualTypeEnum::ACCRUAL])) {
                $this->addAccrual(
                    $userId,
                    AccrualTypeEnum::ACCRUAL,
                    $period,
                    $accruals[AccrualTypeEnum::ACCRUAL]
                );
            }
            if (isset($accruals[AccrualTypeEnum::DEDUCTION])) {
                $this->addAccrual(
                    $userId,
                    AccrualTypeEnum::DEDUCTION,
                    $period,
                    $accruals[AccrualTypeEnum::DEDUCTION]
                );
            }
        }

        return $this->redirect(['/backend/accrual/index', 'period' => $period]);

    }

    public function actionValidateFile()
    {

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $result = [
            'success' => true,
            'massage' => ''
        ];
        $model = new DataUploadForm();

        $data = Yii::$app->request->post();
        if ($model->load($data)) {

            $model->financeFile = UploadedFile::getInstance($model, 'financeFile');
            $result = $model->validateFile($model->period);
        }

        return $result;

    }

    public function actionValidateDate()
    {

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $result = [
            'success' => true,
            'massage' => '',
            'confirm' => ''
        ];
        $model = new DataUploadForm();

        $data = Yii::$app->request->post();
        if ($model->load($data)) {

            $model->financeFile = UploadedFile::getInstance($model, 'financeFile');
            $result = $model->validateFile($model->period);
        }

        return $result;

    }

    /**
     * @param $userWorkId
     * @param $accrualType
     * @param $period
     * @param $accruals
     * @return bool
     * @throws AccrualException
     */
    private function addAccrual($userWorkId, $accrualType, $period, $accruals)
    {
        if (!is_array($accruals)) {
            return false;
        }
        foreach ($accruals as $accrualViewId => $sum) {
            if (!$sum) {
                continue;
            }
            $accrualForm = $this->accrualFormFactory->makeByParameters($userWorkId, $period, $accrualViewId, $accrualType);
            $this->saveAccrualService->save($accrualForm->makeDto());
        }
    }

    /**
     * @param string $period
     * @return string
     */
    private function addPeriod(string $period)
    {
        if (Period::find()->where(['period' => $period])->count()) {
            return $period;
        }
        $model = new Period();
        $model->period = $period;
        $model->save();
        return $period;
    }

}