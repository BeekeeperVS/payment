<?php

return [
    [
        'name' => 'Людмила',
        'surname' => 'Швец',
        'patronymic' => 'Викторовна',
        'work_id' => 1,
        'role' => 1,
        'login' => 'admin',
        'email' => 'test@gmail.com',
        'password' => "123456",
        'first_password' => 'password',
        'is_active' => 1
    ],
    [
        'name' => 'Любовь',
        'surname' => 'Сопилко',
        'patronymic' => 'Анатольевна',
        'work_id' => 55,
        'role' => 2,
        'login' => 'finance_manager',
        'email' => 'test2@gmail.com',
        'password' => "123456",
        'first_password' => 'password',
        'is_active' => 1
    ],
    [
        'name' => 'Дмитрий',
        'surname' => 'Кириченко',
        'patronymic' => '',
        'work_id' => 34,
        'role' => 3,
        'login' => 'hr_manager',
        'email' => 'test3@gmail.com',
        'password' => "123456",
        'first_password' => 'password',
        'is_active' => 1
    ]
];