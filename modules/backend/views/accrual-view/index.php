<?php

use app\components\gridView\ActionFilterColumn;
use app\components\widgets\settingPagePagination\SettingPagePagination;
use app\models\db\AccrualView;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AccrualViewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = translate('Accrual Views');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accrual-view-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--        --><? //= Html::a(translate('Create Accrual View'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= SettingPagePagination::widget(['pagination' => $dataProvider->pagination]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ]
            ],


//            'id',
            'title',
            'cipher',
            [
                'attribute' => 'type_alias',
                'value' => function (AccrualView $model){
                    return $model->typeAlias->title;
                }
            ],

            [
                'class' => ActionFilterColumn::class,
                'template' => '{update}',
                'headerOptions' => [
                    'class' => 'search-actions',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ]
            ],
        ],
    ]); ?>


</div>
