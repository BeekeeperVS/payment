<?php


namespace app\components\factories\subdivision\form;


use app\components\factories\subdivision\services\save\dto\SaveSubdivisionDto;
use app\components\factories\subdivision\services\save\dto\SaveSubdivisionDtoInterface;
use app\models\db\Subdivision;
use yii\base\Model;

class SubdivisionForm extends Model implements SubdivisionFormInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'in', 'not' => true,
                'range' => $this->getListValidateUnique(),
                'message' => translate('Такая должность уже существует')
            ],
            [['name'], 'string', 'max' => 100],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'id' => 'ID',
            'name' => 'Subdivision',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
       return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**'
     * @return SaveSubdivisionDtoInterface
     */
    public function makeDto(): SaveSubdivisionDtoInterface
    {
        return new SaveSubdivisionDto(
            $this->id,
            $this->name
        );
    }


    /**
     * @return string[]
     */
    private function getListValidateUnique(): array
    {
        $list = Subdivision::find()->select('name')->column();

        if ($this->getId()) {
            $nameCurrent = (Subdivision::find()->select('name')->where(['id' => $this->getId()])->column())[0];
            if (($key = array_search($nameCurrent, $list)) !== false) {
                unset($list[$key]);
            }
        }

        return $list;
    }
}