<?php


namespace app\assets;


use yii\web\AssetBundle;

class AccountantPageCryptoAsset extends AssetBundle
{
    public $basePath = '@webroot/';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $css = [

    ];
    public $js = [
        'asset-manager/crypto/js/read_accrual_accountant_page.js',
    ];
    public $depends = [
       CryptoAsset::class,
    ];
}