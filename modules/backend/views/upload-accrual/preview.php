<?php

use app\assets\CreateUpdateCryptoAsset;
use app\assets\CryptoAsset;
use app\components\AccrualTypeEnum;
use app\components\factories\user\view\UserView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $viewsModels UserView[] */
/* @var $period string */

CreateUpdateCryptoAsset::register($this);

$this->title = 'Просмотр загруженого файла 1С';
$this->params['breadcrumbs'][] = $this->title;

$sessionModels = Yii::$app->session->get('uploadInformation');
$users = $sessionModels['users'];
$sessionModelsAccruals = $sessionModels['accruals'];
?>
<div class="accrual-user-page">
    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <div class="container">
        <br>

        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <?php $index = 1; ?>
                    <tr>
                        <th>#</th>
                        <th>Табельный номер</th>
                        <th>ФИО</th>
                        <?php foreach (UserView::getAccrualViewList() as $id => $title) : ?>
                            <th> <?= $title ?></th>
                        <?php endforeach; ?>
                        <th>Удержано</th>
                        <th>Итого</th>
                    </tr>

                    <?php foreach ($viewsModels as $viewsModel) : ?>
                        <?php
                        $userId = $viewsModel->workId;
                        $accrualModel = $sessionModelsAccruals[$userId];
                        ?>
                        <?php if (!isset($accrualModel[AccrualTypeEnum::ACCRUAL])) continue; ?>
                        <tr>
                            <td><?= $index++ ?></td>
                            <td><?= $userId ?></td>
                            <td><?= $users[$userId] ?></td>
                            <?php foreach (UserView::getAccrualViewList() as $id => $title) : ?>
                                <td>
                                    <?= Html::tag('input', '', [
                                        'type' => 'text',
                                        'size' => 10,
                                        'name' => 'accrual',
                                        'value' => $accrualModel[AccrualTypeEnum::ACCRUAL][$id] ?? '0',
                                        'class' => "accrual accrual-$id-$userId accrual-encrypt",
                                        'data' => [
                                            'crypto-key' => "{$userId}_{$period}_{$id}",
                                            'user-id' => $userId,
                                            'accrual-id' => $id,
                                            'url' => Url::to([
                                                '/backend/upload-accrual/edit-accrual'
                                            ]),
                                        ],
                                    ]); ?>
                                </td>
                            <?php endforeach; ?>
                            <td class="sum-<?= AccrualTypeEnum::DEDUCTION . '-' . $userId; ?>">
                                <?= Html::tag('input', '', [
                                    'type' => 'text',
                                    'size' => 10,
                                    'name' => 'deduction-sum',
                                    'disabled' => 'disabled',
                                    'value' => isset($accrualModel[AccrualTypeEnum::DEDUCTION]) ?
                                        array_sum($accrualModel[AccrualTypeEnum::DEDUCTION]) : 0,
                                    'class' => "deduction-sum",
                                ]); ?>
                            </td>
                            <td class="sum-<?= AccrualTypeEnum::ACCRUAL . '-' . $userId; ?>">
                                <?= Html::tag('input', '', [
                                    'type' => 'text',
                                    'size' => 10,
                                    'name' => 'accrual-sum',
                                    'disabled' => 'disabled',
                                    'value' => isset($accrualModel[AccrualTypeEnum::ACCRUAL]) ?
                                        array_sum($accrualModel[AccrualTypeEnum::ACCRUAL]) : 0,
                                    'class' => "accrual-sum accrual-encrypt",
                                    'data' => [
                                        'crypto-key' => "{$userId}_{$period}_sum",
                                    ]
                                ]); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-5">
                <?= Html::tag('a', translate('Save'), [
                    'href' => '#',//Url::to(['/backend/upload-accrual/save']),
                    'class' => 'btn btn-primary',
                    'id' => 'buttonEncrypt',
                ]) ?>
                <?= Html::tag('a', translate('Cancel'), [
                    'href' => Url::to(['/backend/upload-accrual/upload']),
                    'class' => 'btn btn-danger'
                ]) ?>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
    </div>
</div>
