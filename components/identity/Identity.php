<?php


namespace app\components\identity;


use app\models\db\User;
use yii\web\IdentityInterface;

class Identity extends User implements IdentityInterface
{

    /**
     * @inheritdoc
     */
    public static function findIdentity($id): User
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public function validatePassword($password)
    {
        return ($this->validatePasswordHash($password) && $this->is_active);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * @inheritdoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    /**
     * @param array $roles
     * @return bool
     */
    public function whoIs(array $roles): bool
    {
        return in_array($this->role, $roles);

    }

    /**
     * @return bool
     */
    public function isReplacePassword(): bool
    {
        return $this->password === $this->first_password;
    }

    /**
     * @return int|null
     */
    public function getWorkId(): ?int
    {
        return $this->work_id;
    }

    /**
     * @param $password
     * @return bool
     */
    private function validatePasswordHash($password)
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

}