<?php

namespace app\models;

use app\models\db\User;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RestorePasswordForm extends Model
{
    /**
     * @var $email string
     */
    public $email;

    /**
     * @var $email string
     */
    public $login;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email are both required
            [['email'], 'required'],
            // password is validated by validatePassword()
//            [['email'], LoginOrEmailValidator::class],
            [['email'], 'in', 'range' => $this->getEmailLoginListValidate(), 'message' => translate('User with such Email/Login is not registered')],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return translateArray([
            'email' => 'Email',
        ]);
    }

    /**
     * @return array
     */
    private function getEmailLoginListValidate(): array
    {
        $emailList = User::find()->select('email')->column();
        $loginList = User::find()->select('login')->column();

        return array_merge($emailList, $loginList);

    }


    public function sendPassword()
    {
        $user = $this->getUser();
        $newPassword = $this->dischargePassword();
        $message = Yii::$app->mailer->compose()
            ->setFrom(['bortik.work@gmail.com' => 'Finance Manager'])
            ->setTo($user->email)
            ->setSubject('Восстановление пароля')
            ->setHtmlBody($this->getMailHtmlText($user, $newPassword));
        if ($message->send()) {
            $result = ['class' => 'success', 'message' => 'Пароль отправлен на ваш Email'];
            $user->setNewPassword($newPassword);
        } else {
            $result = ['class' => 'danger', 'message' => 'Произошла ошыбка при отправке пароля'];
        }
        Yii::$app->session->setFlash('sendPassword', $result);

        return $result;
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        /** @var User $user */
        if ($user = User::find()->where(['email' => $this->email])->one()) {
        } else {
            $user = User::find()->where(['login' => $this->email])->one();
        }

        return $user;
    }

    /**
     * @param User $user
     * @param string $newPassword
     * @return string
     */
    private function getMailHtmlText(User $user, string $newPassword): string
    {
        return "<h1>Уважаемый(ая) $user->name!</h1>
                <p>Ваш login: $user->login</p>
                <p>Ваш пароль $newPassword</p>";
    }

    /**
     * @return string
     */
    private function dischargePassword(): string
    {
        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $max = 10;
        $size = StrLen($chars) - 1;
        $password = null;
        while ($max--) {
            $password .= $chars[rand(0, $size)];
        }
        return $password;
    }
}
