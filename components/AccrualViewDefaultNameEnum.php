<?php


namespace app\components;


abstract class AccrualViewDefaultNameEnum
{
    public static function getDefaultCipherName(string $accrualViewName)
    {

        $defaultAccrualViewsConfiguration = require __DIR__ . '/init/accrual-view.php';
        foreach ($defaultAccrualViewsConfiguration as $accrualViewDefault) {
            if ($accrualViewDefault['title'] == $accrualViewName) {
                return $accrualViewDefault['cipher'];
            }
        }
        return 'Нужен пустой список шифров начислений';
    }

}