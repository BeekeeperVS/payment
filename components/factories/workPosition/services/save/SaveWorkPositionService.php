<?php


namespace app\components\factories\workPosition\services\save;


use app\components\factories\workPosition\services\save\dto\SaveWorkPositionDtoInterface;
use app\components\workPosition\exception\WorkPositionException;
use app\models\db\WorkPosition;

class SaveWorkPositionService implements SaveWorkPositionServiceInterface
{

    /**
     * @inheritDoc
     */
    public function save(SaveWorkPositionDtoInterface $dto): int
    {
        $model = $dto->getId() ? $this->findWorkPosition($dto) : new WorkPosition();

        $this->updateSubdivision($dto, $model);

        return $model->id;
    }

    /**
     * @param SaveWorkPositionDtoInterface $dto
     * @return WorkPosition
     * @throws WorkPositionException
     */
    private function findWorkPosition(SaveWorkPositionDtoInterface $dto): WorkPosition
    {
        $model = WorkPosition::findOne($dto->getId());
        if($model){
            return $model;
        } else {
            throw new WorkPositionException(SaveWorkPositionServiceInterface::class . ' Failed to find Subdivision by id: ' . $dto->getId());
        }
    }

    /**
     * @param SaveWorkPositionDtoInterface $dto
     * @param WorkPosition $model
     */
    private function updateSubdivision(SaveWorkPositionDtoInterface $dto, WorkPosition $model)
    {
        $model->id = $dto->getId();
        $model->name = $dto->getName();

        $model->save();
    }

}