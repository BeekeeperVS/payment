<?php

namespace app\controllers;

use app\components\RoleTypeEnum;
use app\models\RestorePasswordForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\widgets\ActiveForm;


class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Change language
     * @return mixed
     */

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            $this->goToSite();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->goToSite();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Remind password
     */
    public function actionRestorePassword()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $model = new RestorePasswordForm();

        $data = Yii::$app->request->post();

        if ($model->load($data)) {

            return $model->sendPassword();

        }

    }

    /**
     * Ajax validate restorePasswordForm
     */
    public function actionValidate()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $model = new RestorePasswordForm();

        $model->load(Yii::$app->request->post());

        return ActiveForm::validate($model, ['email']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    private function goToSite()
    {
        if (Yii::$app->user->identity->whoIs([RoleTypeEnum::ADMIN, RoleTypeEnum::FINANCE_MANAGER])) {
            $this->redirect(['/backend/accrual']);
        } elseif (Yii::$app->user->identity->whoIs([RoleTypeEnum::HR_MANAGER])) {
            $this->redirect(['/backend/user']);
        } else {
            $this->redirect(['/backend/accrual/user-page']);
        }
    }
}
