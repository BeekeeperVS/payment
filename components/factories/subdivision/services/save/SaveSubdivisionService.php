<?php


namespace app\components\factories\subdivision\services\save;


use app\components\factories\subdivision\exception\SubdivisionException;
use app\components\factories\subdivision\services\save\dto\SaveSubdivisionDtoInterface;
use app\models\db\Subdivision;

class SaveSubdivisionService implements SaveSubdivisionServiceInterface
{

    /**
     * @inheritDoc
     */
    public function save(SaveSubdivisionDtoInterface $dto): int
    {
        $model = $dto->getId() ? $this->findSubdivision($dto) : new Subdivision();

        $this->updateSubdivision($dto, $model);

        return $model->id;
    }

    private function updateSubdivision(SaveSubdivisionDtoInterface $dto, Subdivision $model)
    {
        $model->id = $dto->getId();
        $model->name = $dto->getName();

        $model->save();
    }

    /**
     * @param SaveSubdivisionDtoInterface $dto
     * @return Subdivision
     * @throws SubdivisionException
     */
    private function findSubdivision(SaveSubdivisionDtoInterface $dto): Subdivision
    {
        $model = Subdivision::findOne($dto->getId());
        if ($model) {
            return $model;
        } else {
            throw new SubdivisionException(SaveSubdivisionServiceInterface::class . ' Failed to find Subdivision by id: ' . $dto->getId());
        }
    }
}