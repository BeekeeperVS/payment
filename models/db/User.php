<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property int $work_id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property int $work_position
 * @property string $email
 * @property string $login
 * @property string $password
 * @property int $role
 * @property int $is_active
 * @property int $is_deleted
 * @property string $first_password
 * @property string $created_at
 * @property string $updated_at
 * @property int $subdivision
 * @property Accrual[] $accruals
 * @property Subdivision $subdivision0
 * @property WorkPosition $workPosition
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'role'], 'required'],
            [['work_id', 'work_position', 'role', 'is_active', 'is_deleted', 'subdivision'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'surname', 'patronymic', 'email', 'login', 'password'], 'string', 'max' => 100],
            [['first_password'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['login'], 'unique'],
            [['subdivision'], 'exist', 'skipOnError' => true, 'targetClass' => Subdivision::className(), 'targetAttribute' => ['subdivision' => 'id']],
            [['work_position'], 'exist', 'skipOnError' => true, 'targetClass' => WorkPosition::className(), 'targetAttribute' => ['work_position' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'id' => 'ID',
            'work_id' => 'Work ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'patronymic' => 'Patronymic',
            'work_position' => 'Work Position',
            'email' => 'Email',
            'login' => 'Login',
            'password' => 'Password',
            'role' => 'Role',
            'is_active' => 'Is Active',
            'is_deleted' => 'Is Deleted',
            'first_password' => 'First Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'subdivision' => 'Subdivision',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccruals()
    {
        return $this->hasMany(Accrual::className(), ['user_work_id' => 'work_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdivision0()
    {
        return $this->hasOne(Subdivision::className(), ['id' => 'subdivision']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkPosition()
    {
        return $this->hasOne(WorkPosition::className(), ['id' => 'work_position']);
    }


    /**
     * @param $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setNewPassword(string $password){

        $this->password = $this->first_password = \Yii::$app->getSecurity()->generatePasswordHash($password);
        $this->save();
    }
}
