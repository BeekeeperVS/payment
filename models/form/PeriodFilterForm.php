<?php


namespace app\models\form;


use app\models\db\Period;
use yii\base\Model;

class PeriodFilterForm extends Model
{
    /**
     * @var string
     */
    public $period_from;

    /**
     * @var string
     */
    public $period_to;

    /**
     * PeriodFilterForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->period_from = Period::getLastPeriodForUser();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['period_from', 'required', 'enableClientValidation' => false],
            [['period_from', 'period_to'], 'string', 'max' => 100, 'enableClientValidation' => false ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'period_from' => 'Start date',
            'period_to' => 'Period To',
        ]);
    }

    public function getPeriod()
    {
        if (!$this->period_to) {
            return [$this->period_from];
        }
        $periodsList = Period::getListByPeriod($this->period_from, $this->period_to);

        return $periodsList;
    }

    /**
     * @param string $periodFrom
     * @param string $periodTo
     */
    public function setPeriods(string $periodFrom, string $periodTo)
    {
        $this->period_from = $periodFrom;
        $this->period_to = $periodTo;
    }
}