<?php


namespace app\models\form;


use app\models\db\Period;
use yii\base\Model;

class AccrualFilterForm extends Model
{
    /**
     * @var int
     */
    public $work_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $period;


    /**
     * PeriodFilterForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->period = Period::getLastPeriod();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['work_id', 'integer', 'enableClientValidation' => false],
            [['name', 'period'], 'string', 'max' => 100, 'enableClientValidation' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'period' => 'Period',
            'name' => 'Name',
            'work_id' => 'Work Id'
        ]);
    }


}