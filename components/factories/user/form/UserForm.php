<?php


namespace app\components\factories\user\form;


use app\components\factories\user\services\save\dto\SaveUserDto;
use app\components\factories\user\services\save\dto\SaveUserDtoInterface;
use app\models\db\User;
use yii\base\Model;

class UserForm extends Model implements UserFormInterface
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $workId;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $surName;
    /**
     * @var string
     */
    public $patronymic;
    /**
     * @var int|null
     */
    public $workPosition;
    /**
     * @var int|null
     */
    public $subdivision;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $login;
    /**
     * @var string
     */
    public $password;
    /**
     * @var int
     */
    public $role;
    /**
     * @var bool
     */
    public $isActive;
    /**
     * @var bool
     */
    public $isDeleted;
    /**
     * @var string
     */
    public $password_repeat;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['workId', 'name', 'surName', 'role',
                'email', 'login', 'password', 'password_repeat'
            ], 'required'],
//            ['password', 'compare', 'compareAttribute' => 'password_repeat' ],
            ['password_repeat', 'compare','compareAttribute' => 'password','message' => translate('Password don\'t match')],
            [['workId', 'role', 'isActive', 'isDelete', 'subdivision', 'workPosition'], 'integer'],
            [['name', 'surName', 'patronymic', 'email', 'login', 'password'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['email'], 'in', 'not' => true,
                'range' => $this->getEmailListValidate(),
                'message' => translate('Такой Email уже существует')],
            [['login'], 'in', 'not' => true,
                'range' => $this->getLoginListValidate(),
                'message' => translate('Такой логин уже существует')],
            [['workId'], 'in', 'not' => true,
                'range' => $this->getWorkIdListValidate(),
                'message' => translate('Такой табельный номер уже существует')],
            ['login', 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z0-9-_\.!=#]{1,100}$/i']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'name' => 'Name*',
            'surName' => 'Surname*',
            'patronymic' => 'Patronymic',
            'workPosition' => 'Work Position',
            'subdivision' => 'Subdivision',
            'workId' => 'Personnel Number*',
            'email' => 'Email*',
            'login' => 'Login*',
            'password' => 'Password*',
            'password_repeat' => 'Password Repeat*',
            'role' => 'Role',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getWorkId(): int
    {
        return $this->workId;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getSurName(): string
    {
        return $this->surName;
    }

    /**
     * @inheritDoc
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @inheritDoc
     */
    public function getWorkPosition(): ?int
    {
        return $this->workPosition;
    }

    /**
     * @inheritDoc
     */
    public function getSubdivision(): ?int
    {
        return $this->subdivision;
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @inheritDoc
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRole(): int
    {
        return $this->role;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function getIsDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return "$this->surName $this->name $this->patronymic";
    }

    public function makeDto(): SaveUserDtoInterface
    {
        return new SaveUserDto(
            (int)$this->id,
            $this->workId ?? null,
            (string)$this->name,
            (string)$this->surName,
            (string)$this->patronymic,
            $this->workPosition,
            $this->subdivision,
            (string)$this->email,
            (string)$this->login,
            (string)$this->password,
            (int)$this->role,
            (bool)$this->isActive,
            (bool)$this->isDeleted
        );
    }

    /**
     * @return array
     */
    private function getEmailListValidate(): array
    {
        $emailList = User::find()->select('email')->column();
        if ($this->getId()) {
            $emailCurrent = (string)(User::find()->select('email')->where(['id' => $this->getId()])->column())[0];
            if (($key = array_search($emailCurrent, $emailList)) !== false) {
                unset($emailList[$key]);
            }
        }
        return $emailList;

    }

    /**
     * @return array
     */
    private function getLoginListValidate(): array
    {
        $loginList = User::find()->select('login')->column();
        if ($this->getId()) {
            $loginCurrent = (User::find()->select('login')->where(['id' => $this->getId()])->column())[0];
            if (($key = array_search($loginCurrent, $loginList)) !== false) {
                unset($loginList[$key]);
            }
        }
        return $loginList;

    }

    /**
     * @return array
     */
    public function getWorkIdListValidate(): array
    {
        $workIdList = User::find()->select('work_id')->column();
        if ($this->getId()) {
            $loginCurrent = (User::find()->select('work_id')->where(['id' => $this->getId()])->column())[0];
            if (($key = array_search($loginCurrent, $workIdList)) !== false) {
                unset($workIdList[$key]);
            }
        }
        return $workIdList;

    }
}