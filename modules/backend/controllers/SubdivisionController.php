<?php

namespace app\modules\backend\controllers;

use app\components\factories\subdivision\exception\SubdivisionException;
use app\components\factories\subdivision\form\factory\SubdivisionFormFactoryInterface;
use app\components\factories\subdivision\services\save\SaveSubdivisionServiceInterface;
use app\components\RoleTypeEnum;
use Yii;
use app\models\search\SubdivisionSearch;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SubdivisionController implements the CRUD actions for Subdivision model.
 */
class SubdivisionController extends Controller
{
    /**
     * @var SubdivisionFormFactoryInterface
     */
    private $subdivisionFormFactory;
    /**
     * @var SaveSubdivisionServiceInterface
     */
    private $saveSubdivisionService;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new NotFoundHttpException();
                },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'create', 'update', 'delete',
                        ],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->user->identity->whoIs([
                                RoleTypeEnum::ADMIN,
                                RoleTypeEnum::FINANCE_MANAGER,
                                RoleTypeEnum::HR_MANAGER
                            ]));
                        }
                    ],
                ]
            ]
        ];
    }

    public function __construct(
        $id,
        Module $module,
        SubdivisionFormFactoryInterface $subdivisionFormFactory,
        SaveSubdivisionServiceInterface $saveSubdivisionService,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->subdivisionFormFactory = $subdivisionFormFactory;
        $this->saveSubdivisionService = $saveSubdivisionService;
    }

    /**
     * Lists all Subdivision models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubdivisionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Subdivision model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     * @throws SubdivisionException
     */
    public function actionCreate()
    {
        $model = $this->subdivisionFormFactory->make();

        if ($model->load(Yii::$app->request->post())) {
            $this->saveSubdivisionService->save($model->makeDto());
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Subdivision model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return string|Response
     * @throws SubdivisionException
     */
    public function actionUpdate($id)
    {
        $model = $this->subdivisionFormFactory->make($id);

        if ($model->load(Yii::$app->request->post())) {
            $this->saveSubdivisionService->save($model->makeDto());
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Subdivision model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }
}
