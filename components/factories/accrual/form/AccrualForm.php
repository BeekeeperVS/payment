<?php


namespace app\components\factories\accrual\form;


use app\components\factories\accrual\services\save\dto\SaveAccrualDto;
use app\components\factories\accrual\services\save\dto\SaveAccrualDtoInterface;
use app\models\db\AccrualType;
use app\models\db\AccrualView;
use app\models\db\User;
use yii\base\Model;

class AccrualForm extends Model implements AccrualFormInterface
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $userWorkId;
    /**
     * @var string
     */
    public $accrualType;
    /**
     * @var int
     */
    public $accrualViewId;
    /**
     * @var string
     */
    public $comment;
    /**
     * @var string
     */
    public $period;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userWorkId', 'accrualType', 'accrualViewId', 'period'], 'required'],
            [['userWorkId', 'accrualViewId'], 'integer'],
            [['accrual_type'], 'string', 'max' => 100],
            [['comment', 'period'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritDoc
     */
    public function getUserWorkId(): int
    {
        return $this->userWorkId;
    }

    /**
     * @inheritDoc
     */
    public function getAccrualType(): string
    {
        return $this->accrualType;
    }

    /**
     * @inheritDoc
     */
    public function getAccrualViewId(): int
    {
        return $this->accrualViewId;
    }

    /**
     * @inheritDoc
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @inheritDoc
     */
    public function getPeriod(): string
    {
        return $this->period;
    }


    /**
     * @return SaveAccrualDtoInterface
     */
    public function makeDto(): SaveAccrualDtoInterface
    {
        return new SaveAccrualDto(
            $this->id,
            (int)$this->userWorkId,
            (string)$this->accrualType,
            (int)$this->accrualViewId,
            (string)$this->comment,
            (string)$this->period
        );
    }
}