<?php

use app\models\form\AccrualFilterForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $filterForm AccrualFilterForm */


?>
<div class="panel panel-default">
    <div class="panel-heading panel-filter">
        <h4 class="panel-title btn-filter">
            <button class="btn btn-filter btn-block" data-action = 'show'>
                <i class="fa fa-filter"></i>
                Фильтр
            </button>
        </h4>
    </div>
    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'action' => Url::to(['/backend/accrual/filter', 'per-page' => $_GET['per-page'] ?? null]),
    ]);
    ?>
    <div class="panel-body filter-accountant hide">
        <div class="row ">
            <div class="col-lg-4">
                <?= $form->field($filterForm, 'work_id')->textInput(); ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($filterForm, 'name')->textInput()->label(translate('Surname')); ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($filterForm, 'period')->widget(DatePicker::class, [
                        'name' => 'dp_3',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'value' => '23-Feb-1982',
                        'removeButton' => false,
                        'pluginOptions' => [
                            'buttonText' => '',
                            'autoclose' => true,
                            'format' => 'mm-yyyy',
                            'startView' => 'year',
                            'minViewMode' => 'months',
                            'endDate' => date('m-Y')
                        ]
                    ]
                ); ?>
            </div>
        </div>
    </div>
    <div class="panel-footer filter-accountant hide">
        <div class="form-group">
            <?= Html::submitButton('Найти', ['class' => '
                    btn btn-primary btn-lg',
            ]) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

