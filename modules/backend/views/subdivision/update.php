<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Subdivision */

$this->title = translate('Update Subdivision').': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => translate('Subdivisions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = translate('Update');
?>
<div class="subdivision-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
