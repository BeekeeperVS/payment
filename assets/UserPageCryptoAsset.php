<?php


namespace app\assets;


use yii\web\AssetBundle;

class UserPageCryptoAsset extends AssetBundle
{
    public $basePath = '@webroot/';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $css = [

    ];
    public $js = [
        'asset-manager/crypto/js/read_accrual_user_page.js',
    ];
    public $depends = [
       CryptoAsset::class,
    ];
}