<?php

use app\components\factories\user\form\UserForm;
use app\components\RoleTypeEnum;
use app\components\SubdivisionEnum;
use app\components\WorkPositionEnum;
use app\models\db\Subdivision;
use app\models\db\WorkPosition;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model UserForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $showReplacePassword bool */
?>
<?php

$identity = Yii::$app->user->identity;
$disabledParameterFromUser = [];
if ($identity->whoIs([RoleTypeEnum::USER])) {
    $disabledParameterFromUser = true;
} else {
    $disabledParameterFromUser = false;
}
$classHide = '';
$model->password_repeat = $model->password;
if ($model->id && !$showReplacePassword) {
    $classHide = 'hide';
}

$attributeLabels = $model->attributeLabels();
?>
<div class="user-form">
    <div class="row">
        <div class="col-md-12">
            <label>ФИО: </label>
            <span><?= $model->getFullName() ?></span>
        </div>
        <div class="col-md-12">
            <label><?= str_replace('*', '', $attributeLabels['workPosition']) ?>: </label>
            <span><?= isset($model->workPosition) ?
                    (WorkPosition::find($model->workPosition)->select('name')->column())[0]
                    : 'не указано'
                ?></span>
        </div>
        <div class="col-md-12">
            <label><?= str_replace('*', '', $attributeLabels['subdivision']) ?>: </label>
            <span><?= isset($model->workPosition) ?
                    (Subdivision::find($model->subdivision)->select('name')->column())[0]
                : 'не указано'
                ?></span>
        </div>
        <div class="col-md-12">
            <label><?= str_replace('*', '', $attributeLabels['email']) ?>: </label>
            <span><?= $model->email ?></span>
        </div>
        <div class="col-md-12">
            <label><?= str_replace('*', '', $attributeLabels['login']) ?>: </label>
            <span><?= $model->login ?></span>
        </div>
    </div>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-lg-4 password <?= $classHide ?>">
            <?= $form->field($model, 'password')->passwordInput([
                'maxlength' => true,
                'class' => 'form-control',
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-4 password <?= $classHide ?>">
            <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton(translate('Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
