<?php

namespace app\models\db;

use app\components\AccrualViewDefaultNameEnum;
use Yii;

/**
 * This is the model class for table "accrual_view".
 *
 * @property int $id
 * @property string $title
 * @property string $cipher
 * @property string $type_alias
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Accrual[] $accruals
 * @property AccrualType $typeAlias
 */
class AccrualView extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accrual_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'type_alias'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'cipher', 'type_alias'], 'string', 'max' => 100],
            [['type_alias'], 'exist', 'skipOnError' => true, 'targetClass' => AccrualType::className(), 'targetAttribute' => ['type_alias' => 'alias']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'id' => 'ID',
            'title' => 'Title',
            'cipher' => 'Cipher',
            'type_alias' => 'Type Alias',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccruals()
    {
        return $this->hasMany(Accrual::className(), ['accrual_view_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeAlias()
    {
        return $this->hasOne(AccrualType::className(), ['alias' => 'type_alias']);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->cipher == "") {
            $this->cipher = AccrualViewDefaultNameEnum::getDefaultCipherName($this->title);
        }
        return parent::save($runValidation, $attributeNames);
    }
}
