<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\WorkPosition */

$this->title = translate('Create Work Position');
$this->params['breadcrumbs'][] = ['label' => translate('Work Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-position-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
