<?php


namespace app\components\factories\user\services\save\dto;


use app\components\RoleTypeEnum;

class SaveUserDto implements SaveUserDtoInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $workId;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $surName;
    /**
     * @var string
     */
    private $patronymic;
    /**
     * @var int|null
     */
    private $workPosition;
    /**
     * @var int|null
     */
    private $subdivision;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $login;
    /**
     * @var string
     */
    private $password;
    /**
     * @var int
     */
    private $role;
    /**
     * @var bool
     */
    private $isActive;
    /**
     * @var bool
     */
    private $isDeleted;

    /**
     * SaveUserDto constructor.
     * @param int|null $id
     * @param int|null $workId
     * @param string $name
     * @param string $surName
     * @param string $patronymic
     * @param int|null $workPosition
     * @param int|null $subdivision
     * @param string $email
     * @param string $login
     * @param string $password
     * @param int $role
     * @param bool $isActive
     * @param bool $isDeleted
     */
    public function __construct(
        ?int $id,
        ?int $workId,
        string $name,
        string $surName,
        string $patronymic,
        ?int $workPosition,
        ?int $subdivision,
        string $email,
        string $login,
        string $password,
        int $role,
        bool $isActive,
        bool $isDeleted
    )
    {
        $this->id = $id;
        $this->workId = $workId;
        $this->name = $name;
        $this->surName = $surName;
        $this->patronymic = $patronymic;
        $this->workPosition = $workPosition;
        $this->email = $email;
        $this->login = $login;
        $this->password = $password;
        $this->role = $role;
        $this->isActive = $isActive;
        $this->isDeleted = $isDeleted;
        $this->subdivision = $subdivision;
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getWorkId(): ?int
    {
        return $this->workId ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getSurName(): string
    {
        return $this->surName;
    }

    /**
     * @inheritDoc
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @inheritDoc
     */
    public function getWorkPosition(): ?int
    {
        return $this->workPosition;
    }

    /**
     * @inheritDoc
     */
    public function getSubdivision(): ?int
    {
        return $this->subdivision;
    }
    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @inheritDoc
     */
    public function getPassword(): ?string
    {
        return $this->password ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getRole(): int
    {
        return $this->role ?? RoleTypeEnum::USER;
    }
    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function getIsDeleted(): bool
    {
        return $this->isDeleted ?? false;
    }

}