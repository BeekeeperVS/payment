$(function () {
    var localValue = JSON.parse(localStorage.getItem('financeManagerCrypt'));
    if(!localValue){
        alert('Необходимо ввести ваш PrivetKey');
        window.location = '/backend/user/profile';
    }
    var privetKey = localValue.privetKey;
    var userLogin = 'alexandr';//localValue.userLogin;
    var applicationAlias = 'Demo'; //localValue.applicationAlias;


    $('#buttonEncrypt').click(function () {
        createUpdateCrypto();
    });

    function createUpdateCrypto() {
        var dataObjects = getObjectsEncrypt();
        console.log(dataObjects);
        var encrypt = new EncryptModel({
            'inputs': {
                privateKey: privetKey,
                objects: dataObjects,
                data: '',
                applications__alias: applicationAlias,
                users_login: userLogin,
                method: 'createUpdate'
            },
            cryptAes: CryptoJS,
            cryptRsa: new JSEncrypt()
        });

        var crypt = encrypt.encrypt();
        var form = [
            {name: "applications__alias", value: applicationAlias},
            {name: "users_login", value: userLogin},
            {name: "method", value: 'createUpdate'},
            {name: "data", value: crypt}
        ];
        $.ajax({
            url: 'http://encpass2.ukrtech.info/api.php',
            method: 'POST',
            data: form,
            success: function (data) {
            }
        })
    }

});

function getObjectsEncrypt() {
    var inputs = $('input.accrual-encrypt');
    var objectsEncrypt = [];
    for (var i = 0; i <= inputs.length; i++) {
        var input = $(inputs[i]);
        var key_id = input.data('crypto-key');
        var sum = input.val();
        if (sum === "0") {
            continue;
        }
        objectsEncrypt.push({'alias': 'accrual-bortic-test', 'key_id': key_id, 'properties': {'sum': sum}})
    }

    return JSON.stringify(objectsEncrypt);
}


