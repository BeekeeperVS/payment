<?php


namespace app\components\factories\user\form\factory;


use app\components\factories\user\form\UserForm;

interface UserFormFactoryInterface
{

    /**
     * @param int|null $id
     * @return UserForm
     */
    public function make(int $id = null): UserForm;
}