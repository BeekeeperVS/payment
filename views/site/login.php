<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use app\models\RestorePasswordForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="logo">
        <h1>Finance Manager</h1>
    </div>
    <div class="login-box">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'login-form',],

            'fieldConfig' => [
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>
        <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i><?= translate('SIGN IN SITE') ?></h3>
        <div class="form-group">
            <label class="control-label"><?= translate('Login') ?></label>
            <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false) ?>
        </div>
        <div class="form-group">
            <label class="control-label"><?= translate('Password') ?></label>
            <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
        </div>
        <div class="form-group">
            <?= Html::button(translate('Restore password'), [
                'type' => 'button',
                'class' => 'btn btn-link',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#restorePassword',

                ],
            ]) ?>
        </div>
        <div class="form-group">
            <div class="utility">
                <div class="animated-checkbox">
                    <?= $form->field($model, 'rememberMe')->checkbox()
                        ->label('<span class="label-text">' . translate('Remember Me') . '</span>', ['style' => 'padding-left: 0;']) ?>
                </div>
            </div>
        </div>
        <div class="form-group btn-container">
            <?= Html::submitButton('<i class="fa fa-sign-in fa-lg fa-fw"></i>' . translate('Sign in'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?= $this->render('_modal-restore-password', ['model' => new RestorePasswordForm()]); ?>