<?php

namespace app\models\form;

use app\components\PeriodEnum;
use app\components\validators\FileDateValidator;
use app\components\xlsFinanceParser\XlsFinanceParser;
use app\models\db\Period;
use yii\base\Model;
use yii\web\UploadedFile;

class DataUploadForm extends Model
{
    const FILE_PATH = 'upload/finance-file.xls';
    /**
     * @var string
     */
    public $period;

    /**
     * DataUploadForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->period = date('m-Y');
    }

    /**
     * @var UploadedFile
     */
    public $financeFile;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['period'], 'required'],
            [['financeFile'], 'required', 'message' => translate('Add 1C file')],
            [['period', 'financeFile'], FileDateValidator::class],
            [['financeFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'period' => 'Period',
            'financeFile' => '1C File',
        ]);
    }

    public function save()
    {
        $this->financeFile->saveAs(self::FILE_PATH);
        return XlsFinanceParser::parseXls(self::FILE_PATH, $this->period);
    }

    /**
     * validate file "is true period" & "was there a downloaded file"
     * @param string $period
     * @return mixed
     */
    public function validateFile($period)
    {
        $this->financeFile->saveAs(self::FILE_PATH);
        $titleFiles = XlsFinanceParser::getTitleFile(self::FILE_PATH);
        if(strpos($titleFiles, $this->getStringPeriod($period)) !== false) {
            $result = [
                'success' => true,
                'message' => '',
                'confirm' => ''
            ];
        } else {
            $result = [
                'success' => false,
                'message' => 'Выбранный период не совпадает c периодом в файле',
                'confirm' => ''
            ];
        }
        if (date("m-Y", strtotime("-1 months")) === $this->period) {
            $result['confirm'] = 'Вы уверены что хотите загрузить файл за предыдущий период?';

        }
        $this->deleteFile();
        return $result;

    }

    /** delete upload file */
    private function deleteFile()
    {
        if (file_exists(self::FILE_PATH)) {
            unlink(self::FILE_PATH);
        }
    }

    private function getStringPeriod(string $period)
    {
        $date = explode('-', $period);
        $date[0] = PeriodEnum::getMonth($date[0]);
        return implode(" ", $date);
    }
}