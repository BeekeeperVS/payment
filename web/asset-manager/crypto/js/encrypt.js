class EncryptModel {
    _randomPassword = null;
    _privateKey = null;
    inputs = {
        privateKey : null
    };

    iv = null;
    salt = null;
    ciphertext = null;

    cryptAes = null;
    cryptRsa = null;

    dataEncrypted = {};

    constructor(config) {
        let key;
        for (key in config) {
            this[key] = config[key];
        }
    }

    get randomPassword () {
        if (this._randomPassword == null) {
            this._randomPassword = 'kjlkjsdsofsojfs'
        }
        return this._randomPassword;
    }

    get privateKey () {
        // if (this._privateKey == null)
        // {
        //     this._privateKey = this.getElement(this.inputs.privateKey).value;
        // }
        // return this._privateKey;
        return this.inputs.privateKey;
    }

    get objects () {
        return this.inputs.objects;
        // return this.getElement(this.inputs.objects).value;
    }

    get applications__alias () {
        // return this.getElement(this.inputs.applications__alias).value;
        return this.inputs.applications__alias;
    }

    get users_login () {
        // return this.getElement(this.inputs.users_login).value;
        return this.inputs.users_login;
    }

    get method () {
        // return this.getElement(this.inputs.method).value;
        return this.inputs.method;
    }

    encryptAes () {
        var CryptoJSAesJson = {
            stringify: function (cipherParams) {
                var j = {ciphertext: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
                if (cipherParams.iv) j.iv = cipherParams.iv.toString();
                if (cipherParams.salt) j.salt = cipherParams.salt.toString();
                return j;
            },
            parse: function (j) {
                var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
                if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
                if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
                return cipherParams;
            }
        };

        let encrypted = this.cryptAes.AES.encrypt(JSON.stringify(this.objects), this.randomPassword, {format: CryptoJSAesJson}).toString();
        this.iv = encrypted.iv;
        this.salt = encrypted.salt;
        this.ciphertext = encrypted.ciphertext;
    }

    encryptRsa () {
        this.cryptRsa.setPrivateKey(this.privateKey);
        this.dataEncrypted['ciphertext'] = this.ciphertext;
        this.dataEncrypted['password'] = this.cryptRsa.encrypt(this.randomPassword);
        this.dataEncrypted['iv'] = this.cryptRsa.encrypt(this.iv);
        this.dataEncrypted['salt'] = this.cryptRsa.encrypt(this.salt);
        this.dataEncrypted['applications__alias'] = this.cryptRsa.encrypt(this.applications__alias);
        this.dataEncrypted['users_login'] = this.cryptRsa.encrypt(this.users_login);
        this.dataEncrypted['method'] = this.cryptRsa.encrypt(this.method);
    }

    writeToInput () {
        var crypt = JSON.stringify(this.dataEncrypted);
        return crypt;

    }

    getElement (id) {
        return window.document.getElementById(id)
    }

    encrypt () {
        this.encryptAes();
        this.encryptRsa();
        return this.writeToInput();
    }
};