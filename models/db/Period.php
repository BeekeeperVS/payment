<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "period".
 * @property int $id
 * @property string $period
 * @property string $created_at
 * @property string $updated_at
 */
class Period extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%period}}';
    }

    /**
     * @return array
     */
    public static function getList(): array
    {
        $models = self::find()->orderBy('period')->all();
        $list = [];
        foreach ($models as $model) {
            $list[] = $model->period;
        }
        return $list;
    }

    /**
     * @return string
     */
    public static function getLastPeriod(): string
    {
        $list = self::find()->select('period')->orderBy('period')->column();
        self::sortPeriodList($list);
        return $list[count($list)-1];
    }

    /**
     * @return string
     */
    public static function getLastPeriodForUser(): string
    {
        $list = self::find()->select('period')->orderBy('period')->column();
        self::sortPeriodList($list);
        for($i= count($list)-1; $i >= 0; $i--){
            if(Accrual::find()->where(['user_work_id' => Yii::$app->user->identity->getWorkId(), 'period' => $list[$i]])->one()){
                return $list[$i];
            }
        }
        return $list[0];
    }
    /**
     * @param string $start
     * @param string $end
     * @return array
     */
    public static function getListByPeriod(string $start, string $end): array
    {

        $list = self::find()->select('period')->orderBy('period')->column();
        self::sortPeriodList($list);
        if(self::comparePeriod($start, $list[count($list)-1]) || self::comparePeriod($start, $end)) {
            return [];
        }
        /**
         * @var  int $startPosition
         * @var  int $endPosition
         */
        if (in_array($start, $list)) {
            foreach ($list as $key => $value) {
                if ($start == $value) {
                    $startPosition = $key;
                    break;
                }
            }
        } else {
            foreach ($list as $key => $value) {
                if (!self::comparePeriod($start, $value)) {
                    $startPosition = $key;
                    break;
                }
            }
        }
        if (in_array($end, $list)) {
            foreach ($list as $key => $value) {
                if ($end == $value) {
                    $endPosition = $key;
                    break;
                }
            }
        } else {
            foreach ($list as $key => $value) {
                if (self::comparePeriod($value, $end)) {
                    $endPosition = $key-1;
                    break;
                }
            }
        }

        $startPosition = $startPosition ?? 0;
        $endPosition = $endPosition ?? (count($list) - 1);

        $listResult = array_slice($list, $startPosition, $endPosition - $startPosition + 1);

        return $listResult;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['period'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['period'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'id' => 'ID',
            'period' => 'Period',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ]);
    }

    /**
     * @param string $start
     * @param string $end
     * @return bool
     */
    private static function comparePeriod($start, $end)
    {
        $data1 = explode('-', $start);
        $data2 = explode('-', $end);

        if ($data1[1] == $data2[1]) {
            if ($data1[0] > $data2[0]) {
                return true;
            } else {
                return false;
            }
        } elseif ($data1[1] > $data2[1]) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param array $listPeriod
     */
    private static function sortPeriodList(array &$listPeriod) {
        $length = count($listPeriod);
        for ($i = 0; $i < $length - 1; $i++) {
            for ($j = $i + 1; $j < $length; $j++) {
                if (self::comparePeriod($listPeriod[$i], $listPeriod[$j])) {
                    $a = $listPeriod[$i];
                    $listPeriod[$i] = $listPeriod[$j];
                    $listPeriod[$j] = $a;
                }
            }
        }
    }
}
