<?php

use app\components\AccrualTypeEnum;
use app\components\factories\user\view\UserView;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $viewModel UserView
 * @var $period string
 */
?>
<?php if (isset($viewModel->accruals[$period])): ?>
    <div class="col-lg-6 crypto-accrual hide " style="margin-bottom: 20px;">
        <caption><i>Начисления за <?= $period ?></i></caption>
        <table class="table table-condensed table-striped accrual-decrypt"
               data-user-key="<?= $viewModel->workId . '_' . $period ?>">
            <tr>
                <th>Вид начислений</th>
                <th>Начислено</th>
                <!--                <th> Удержано</th>-->
            </tr>
            <?php foreach ($viewModel->accruals[$period] as $accrualTitle => $sum) : ?>
                <tr>
                    <?= Html::tag('td', $accrualTitle); ?>
                    <?= Html::tag('td', $sum[AccrualTypeEnum::ACCRUAL] ?? '0', [
                        'class' => [$viewModel->workId . '_' . $period, 'accrual-value'],
                        'data' => [
                            'crypto-key' => $sum[AccrualTypeEnum::ACCRUAL]
                        ]
                    ]); ?>
                    <!--                    <td>--><? //= $sum[AccrualTypeEnum::DEDUCTION] ?? '0' ?><!--</td>-->
                </tr>
            <?php endforeach; ?>
            <tr>
                <td>Итого</td>
                <td class="<?= $viewModel->workId . '_' . $period ?>"
                    data-crypto-key="<?= $viewModel->workId . '_' . $period ?>_sum">
                </td>
                <!--                <td>--><? //= $viewModel->sum[$period][AccrualTypeEnum::DEDUCTION]; ?><!--</td>-->
            </tr>
        </table>
    </div>
<?php endif; ?>
