<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "accrual_type".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Accrual[] $accruals
 * @property AccrualView[] $accrualViews
 */
class AccrualType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accrual_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'alias'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'alias'], 'string', 'max' => 100],
            [['title'], 'unique'],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return translateArray([
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccruals()
    {
        return $this->hasMany(Accrual::className(), ['accrual_type' => 'alias']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccrualViews()
    {
        return $this->hasMany(AccrualView::className(), ['type_alias' => 'alias']);
    }
}
