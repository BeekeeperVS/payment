<?php


namespace app\components\factories\subdivision\services\save\dto;


interface SaveSubdivisionDtoInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string
     */
    public function getName(): string;
}