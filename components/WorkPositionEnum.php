<?php


namespace app\components;


use app\models\db\WorkPosition;

abstract class WorkPositionEnum
{
    public static function getWorkPositionList()
    {
        $list = WorkPosition::find()->all();

        foreach ($list as $workPosition){
            $result[$workPosition->id] = $workPosition->name;
        }

        return $result;
    }
}