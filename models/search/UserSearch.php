<?php

namespace app\models\search;

use app\models\db\WorkPosition;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\User;

/**
 * UserSearch represents the model behind the search form of `app\models\db\User`.
 */
class UserSearch extends User
{
    /**
     * @var bool
     */
    private $isDeleted;

    public function __construct(
        bool $isDeleted = false,
        $config = []
    )
    {
        parent::__construct($config);
        $this->isDeleted = $isDeleted;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'work_id', 'role', 'work_position'], 'integer'],
            [['name', 'surname', 'patronymic',
                'email', 'login', 'password', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->where(['is_deleted' => $this->isDeleted]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'work_id' => $this->work_id,
            'role' => $this->role,
            'work_position' => $this->work_position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'patronymic', $this->patronymic])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }

    public function getWorkPositionsList()
    {

        $list = WorkPosition::find()->select(['id','name'])->all();
        $searchList = [];
        foreach ($list as $workPosition){
            $searchList[$workPosition->id] = $workPosition->name;
        }
        return $searchList;
    }
}
