<?php

namespace app\modules\backend\controllers;

use app\components\factories\user\form\factory\UserFormFactoryInterface;
use app\components\factories\user\services\save\SaveUserServiceInterface;
use app\components\RoleTypeEnum;
use app\modules\backend\Module;
use Yii;
use app\models\db\User;
use app\models\search\UserSearch;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @var UserFormFactoryInterface
     */
    private $userFormFactory;
    /**
     * @var SaveUserServiceInterface
     */
    private $saveUserService;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new NotFoundHttpException();
                },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'update'],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->user->identity->whoIs([
                                RoleTypeEnum::ADMIN,
                                RoleTypeEnum::FINANCE_MANAGER,
                                RoleTypeEnum::HR_MANAGER
                            ]));
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'basket', 'restore', 'active', 'in-basket', 'view'],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->user->identity->whoIs([
                                RoleTypeEnum::ADMIN,
                                RoleTypeEnum::HR_MANAGER
                            ]));
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['edit-work-id'],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->user->identity->whoIs([
                                RoleTypeEnum::FINANCE_MANAGER,

                            ]));
                        }
                    ],
                    [
                        'actions' => ['profile', 'update-profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ]
        ];
    }

    /**
     * UserController constructor.
     * @param $id
     * @param Module $module
     * @param UserFormFactoryInterface $userFormFactory
     * @param SaveUserServiceInterface $saveUserService
     * @param array $config
     */
    public function __construct(
        $id,
        Module $module,
        UserFormFactoryInterface $userFormFactory,
        SaveUserServiceInterface $saveUserService,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->userFormFactory = $userFormFactory;
        $this->saveUserService = $saveUserService;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->userFormFactory->make($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->userFormFactory->make();//new User();

        if ($model->load(Yii::$app->request->post())) {//&& $model->save()) {
            $id = $this->saveUserService->save($model->makeDto());
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @param bool $showReplacePassword
     * @return string|Response
     */
    public function actionUpdate($id, $showReplacePassword = false)
    {
        $model = $this->userFormFactory->make($id);

        if ($model->load(Yii::$app->request->post())) {
            $id = $this->saveUserService->save($model->makeDto());
            $action = Yii::$app->user->identity->whoIs([RoleTypeEnum::USER]) ? 'profile' : 'index';
            return $this->redirect([$action]);
        }

        return $this->render('update', [
            'model' => $model,
            'showReplacePassword' => $showReplacePassword
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return string
     */
    public function actionBasket()
    {
        $searchModel = new UserSearch(true);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('basket', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionInBasket($id)
    {
        $model = $this->userFormFactory->make($id);
        $model->isDeleted = true;
        $model->isActive = false;
        $this->saveUserService->save($model->makeDto());
        return $this->redirect(['index']);

    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionRestore($id)
    {
        $model = $this->userFormFactory->make($id);
        $model->isDeleted = false;
        $this->saveUserService->save($model->makeDto());
        return $this->redirect(['basket']);

    }

    public function actionProfile()
    {
        return $this->render('profile', [
            'model' => $this->userFormFactory->make(Yii::$app->user->identity->getId())
        ]);
    }

    public function actionUpdateProfile($id)
    {
        if(!Yii::$app->user->identity->whoIs([RoleTypeEnum::USER])) {
            return $this->redirect(['update', 'id' => $id, 'showReplacePassword' => true]);
        }

        $model = $this->userFormFactory->make($id);

        if ($model->load(Yii::$app->request->post())) {
            $id = $this->saveUserService->save($model->makeDto());
            return $this->redirect(['profile']);
        }

        return $this->render('update-profile', [
            'model' => $model,
        ]);
    }


    /**
     * @return array
     */
    public function actionActive()
    {
        $resultActive = [
            'class' => 'btn btn-lg btn-primary btn-active',
            'value' => translate('Active'),
        ];
        $resultBlock = [
            'class' => 'btn btn-lg btn-default btn-active',
            'value' => translate('Block'),
        ];
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $id = $_GET['id'];
        $model = $this->userFormFactory->make($id);
        $model->isActive = $model->isActive ? false : true;
        $this->saveUserService->save($model->makeDto());
        return !$model->isActive ? $resultActive : $resultBlock;

    }

    /**
     * @return array
     */
    public function actionEditWorkId()
    {
        $result = [
            'success' => true,
        ];
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $id = $_POST['id'];
        $workId = $_POST['workId'];

        $model = $this->userFormFactory->make($id);
        $workIdList = $model->getWorkIdListValidate();
        if(array_search($workId, $workIdList) !== false){
            $result['success'] = false;
            $result['message'] = 'Пользователь с таким табельным номером уже существует';
            return $result;
        }
        $model->workId = (int)$workId;
        $this->saveUserService->save($model->makeDto());
        return $result;

    }
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
