<?php


namespace app\components\factories\accrual\form\factory;


use app\components\factories\accrual\exception\AccrualException;
use app\components\factories\accrual\form\AccrualForm;

interface AccrualFormFactoryInterface
{

    /**
     * @param int|null $id
     * @return AccrualForm
     * @throws AccrualException
     */
    public function make(int $id = null): AccrualForm;

    /**
     * @param int $userWorkId
     * @param string $period
     * @param int $accrualViewId
     * @param string $accrualType
     * @return AccrualForm
     */
    public function makeByParameters(int $userWorkId, string $period, int $accrualViewId, string $accrualType): AccrualForm;


    /**
     * @param int $userWorkId
     * @param string $period
     * @return AccrualForm[]
     */
    public function makeFromUser(int $userWorkId, string $period): array ;

}